package zkyubz.huskimc.core.huskicore.managers.holograms.tasks;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zkyubz.huskimc.core.huskicore.managers.holograms.HologramManager;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HoloPlayer;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HuskiHologram;

import java.util.Collection;

public class HoloLineUpdateTask extends BukkitRunnable {
    private final HologramManager hologramManager;
    private final HuskiHologram huskiHologram;
    private final String[] lines;

    private final int lineNumber;
    private int lastIndex;

    public HoloLineUpdateTask(HologramManager hologramManager, HuskiHologram huskiHologram, String[] lines, int lineNumber){
        this.hologramManager = hologramManager;
        this.huskiHologram = huskiHologram;
        this.lines = lines;
        this.lineNumber = lineNumber;
        this.lastIndex = 0;
    }

    @Override
    public void run() {
        Collection<HoloPlayer> holoPlayerList = hologramManager.getPlayerList().values();

        if (lastIndex == lines.length){
            this.lastIndex = 0;
        }

        huskiHologram.setLine(lineNumber, lines[lastIndex]);

        for (HoloPlayer holoPlayer : holoPlayerList){
            Player player = holoPlayer.getPlayer();
            huskiHologram.playerUpdateLine(player, lineNumber);
        }

        this.lastIndex++;
    }
}
