package zkyubz.huskimc.core.huskicore.managers.holograms.variables;

import io.netty.channel.ChannelHandlerContext;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.events.packets.HoloClickEvent;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedUseEntity;
import zkyubz.huskimc.core.huskicore.utils.variables.PacketInRunnable;

import java.util.List;

public class HoloClickRunnable implements PacketInRunnable {
    private final HoloButton holoButton;

    public HoloClickRunnable(HoloButton holoButton) {
        this.holoButton = holoButton;
    }

    @Override
    public void execute(Player player, ChannelHandlerContext channelHandlerContext, WrappedPacket wrappedPacket) {
        if (wrappedPacket instanceof WrappedUseEntity){
            if (((WrappedUseEntity) wrappedPacket).getAction() == 2){
                int entityId = ((WrappedUseEntity) wrappedPacket).getEntityId();
                if (entityId == -1) return;

                for (Integer buttonEntityId : holoButton.getEntityIdList()){
                    if (entityId == buttonEntityId){
                        HoloClickEvent event = new HoloClickEvent(player, holoButton.getHuskiHologram(), entityId);
                        Bukkit.getServer().getPluginManager().callEvent(event);

                        List<HoloButtonAction> actionList = holoButton.getActionList();
                        for (int i = 0; i < actionList.size(); i++){
                            actionList.get(i).run(holoButton.getHuskiHologram(), player);
                        }
                        return;
                    }
                }
            }
        }
    }
}
