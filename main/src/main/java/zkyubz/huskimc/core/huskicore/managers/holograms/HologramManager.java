package zkyubz.huskimc.core.huskicore.managers.holograms;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.holograms.tasks.HoloAnimTask;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HoloPlayer;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HuskiHologram;
import zkyubz.huskimc.core.huskicore.managers.player.PlayerTrackerManager;
import zkyubz.huskimc.core.huskicore.managers.variables.AnimText;

import java.util.*;

public class HologramManager {
    private final Map<UUID, HuskiHologram> hologramList;                            //HoloUUID, Hologram
    private final Map<UUID, Integer> lineUpdateTaskIdList;                          //HoloUUID, Task Id
    private final Map<UUID, HoloPlayer> playerList;                                 //PlayerUUID, Player

    public HologramManager(PlayerTrackerManager playerTrackerManager){
        HoloRunnable holoRunnable = new HoloRunnable(this);
        playerTrackerManager.addRunnable(holoRunnable);

        this.hologramList = new HashMap<>();
        this.lineUpdateTaskIdList = new HashMap<>();
        this.playerList = new HashMap<>();
    }

    public UUID addHologram(UUID worldUUID, double x, double y, double z, List<String> lineList){
        HuskiHologram huskiHologram = new HuskiHologram(worldUUID, x, y, z, lineList);
        UUID holoUUID = UUID.nameUUIDFromBytes(new byte[]{(byte) huskiHologram.hashCode()});

        huskiHologram.setHoloUUID(holoUUID);
        hologramList.put(holoUUID, huskiHologram);

        return holoUUID;
    }

    public UUID addHologram(UUID worldUUID, double x, double y, double z, List<String> lineList, boolean spawnByDefault){
        HuskiHologram huskiHologram = new HuskiHologram(worldUUID, x, y, z, lineList, spawnByDefault);
        UUID holoUUID = UUID.nameUUIDFromBytes(new byte[]{(byte) huskiHologram.hashCode()});

        huskiHologram.setHoloUUID(holoUUID);
        hologramList.put(holoUUID, huskiHologram);

        return holoUUID;
    }

    public UUID addHologram(UUID worldUUID, double x, double y, double z, List<String> lineList, boolean spawnByDefault, boolean interactive){
        HuskiHologram huskiHologram = new HuskiHologram(worldUUID, x, y, z, lineList, spawnByDefault, interactive);
        UUID holoUUID = UUID.nameUUIDFromBytes(new byte[]{(byte) huskiHologram.hashCode()});

        huskiHologram.setHoloUUID(holoUUID);
        hologramList.put(holoUUID, huskiHologram);

        return holoUUID;
    }

    //Todo: Test
    public boolean removeHologram(UUID holoUUID){
        HuskiHologram huskiHologram = hologramList.get(holoUUID);

        if (huskiHologram != null){
            removeHoloAnimation(holoUUID);

            //Despawn hologram for all nearby players
            double holoX = huskiHologram.getX();
            double holoY = huskiHologram.getY();
            double holoZ = huskiHologram.getZ();

            for (Player player : Bukkit.getOnlinePlayers()){
                double playerX = player.getLocation().getX();
                double playerY = player.getLocation().getY();
                double playerZ = player.getLocation().getZ();

                if (isNear(playerX, playerY, playerZ, holoX, holoY, holoZ)){
                    HoloPlayer holoPlayer = playerList.get(player.getUniqueId());
                    huskiHologram.despawnHologram(player);
                    holoPlayer.removeHologramVisible(holoUUID);
                }
            }
            hologramList.remove(holoUUID);
            return true;
        }
        else {
            return false;
        }
    }

    public void updateHologramLoc(UUID holoUUID, double x, double y, double z){
        HuskiHologram huskiHologram = hologramList.get(holoUUID);
        if (huskiHologram != null){
            updateHologramLoc(holoUUID, huskiHologram, huskiHologram.getWorldUUID(), x, y, z);
        }
    }

    public void updateHologramLoc(UUID holoUUID, UUID worldUUID, double x, double y, double z){
        HuskiHologram huskiHologram = hologramList.get(holoUUID);
        if (huskiHologram != null){
            updateHologramLoc(holoUUID, huskiHologram, worldUUID, x, y, z);
        }
    }

    public void updateHologramLoc(UUID holoUUID, HuskiHologram huskiHologram, UUID worldUUID, double x, double y, double z){
        huskiHologram.setNewLocation(worldUUID, x, y, z);

        for (HoloPlayer holoPlayer : playerList.values()){
            if (holoPlayer.containsHologram(holoUUID)){
                Player player = holoPlayer.getPlayer();
                double playerX = player.getLocation().getX();
                double playerY = player.getLocation().getY();
                double playerZ = player.getLocation().getZ();

                if (isNear(playerX, playerY, playerZ, x, y, z)){
                    huskiHologram.playerUpdateLoc(player);
                }
                else {
                    huskiHologram.despawnHologram(player);
                    holoPlayer.removeHologramVisible(holoUUID);
                }
            }
        }
    }

    public void updateHologram(UUID holoUUID, UUID worldUUID, double x, double y, double z, List<String> lineList){
        HuskiHologram huskiHologram = hologramList.get(holoUUID);
        if (huskiHologram != null){
            for (HoloPlayer holoPlayer : playerList.values()){
                if (holoPlayer.containsHologram(holoUUID)){
                    huskiHologram.despawnHologram(holoPlayer.getPlayer());
                    holoPlayer.removeHologramVisible(holoUUID);
                }
            }

            removeHoloAnimation(holoUUID);

            huskiHologram.setNewLocation(worldUUID, x, y, z);
            huskiHologram.replaceAll(lineList);

            for (HoloPlayer holoPlayer : playerList.values()){
                spawnIfNear(holoPlayer.getPlayer());
            }
        }
    }

    public void animateHolo(UUID holoUUID, Map<Integer, AnimText> animLines){
        HuskiHologram huskiHologram = hologramList.get(holoUUID);

        if (huskiHologram != null){
            removeHoloAnimation(holoUUID);
            HoloAnimTask task = new HoloAnimTask(this, huskiHologram, animLines);

            int taskId = task.runTaskTimerAsynchronously(HCore.getInstance(), 1L, 1L).getTaskId();

            lineUpdateTaskIdList.put(holoUUID, taskId);
        }
    }

    public void removeHoloAnimation(UUID holoUUID){
        Integer taskId = lineUpdateTaskIdList.get(holoUUID);
        if (taskId != null){
            Bukkit.getScheduler().cancelTask(taskId);
            lineUpdateTaskIdList.remove(holoUUID);
        }
    }

    public void addPlayer(Player player){
        playerList.put(player.getUniqueId(), new HoloPlayer(player));
        spawnIfNear(player);
    }

    public void removePlayer(Player player){
        playerList.remove(player.getUniqueId());
    }

    public void spawnIfNear(Player player){
        for (Map.Entry<UUID, HuskiHologram> entry : hologramList.entrySet()) {
            UUID holoUUID = entry.getKey();
            HuskiHologram huskiHologram = entry.getValue();
            HoloPlayer holoPlayer = playerList.get(player.getUniqueId());

            if (huskiHologram.getWorldUUID() == player.getWorld().getUID()){
                double playerX = player.getLocation().getX();
                double playerY = player.getLocation().getY();
                double playerZ = player.getLocation().getZ();
                double holoX = huskiHologram.getX();
                double holoY = huskiHologram.getY();
                double holoZ = huskiHologram.getZ();

                boolean playerNear = isNear(playerX, playerY, playerZ, holoX, holoY, holoZ);
                boolean containsHolo = holoPlayer.containsHologram(holoUUID);

                if (!containsHolo && playerNear && huskiHologram.spawnByDefault()){
                    huskiHologram.spawnHologram(player);
                    holoPlayer.addHologramVisible(holoUUID);
                }
                else if (containsHolo && !playerNear){
                    huskiHologram.despawnHologram(player);
                    holoPlayer.removeHologramVisible(holoUUID);
                }
            }
        }
    }

    public boolean isNear(double playerX, double playerY, double playerZ, double holoX, double holoY, double holoZ){
        boolean isHoloXNegative = false;
        boolean isHoloYNegative = false;
        boolean isHoloZNegative = false;

        if (holoX < 0){
            isHoloXNegative = true;
        }
        if (holoY < 0){
            isHoloYNegative = true;
        }
        if (holoZ < 0){
            isHoloZNegative = true;
        }

        boolean isXNegative = false;
        boolean isYNegative = false;
        boolean isZNegative = false;

        if (playerX < 0){
            isXNegative = true;
        }
        if (playerY < 0){
            isYNegative = true;
        }
        if (playerY < 0){
            isZNegative = true;
        }

        double distanceX;
        double distanceY;
        double distanceZ;

        if (isHoloXNegative){
            if (isXNegative){
                if (holoX > playerX){
                    distanceX = holoX - playerX;
                }
                else {
                    distanceX = playerX - holoX;
                }
            }
            else {
                distanceX = playerX - holoX;
            }
        }
        else {
            if (isXNegative){
                distanceX = holoX - playerX;
            }
            else {
                if (holoX > playerX){
                    distanceX = holoX - playerX;
                }
                else {
                    distanceX = playerX - holoX;
                }
            }
        }

        if (isHoloYNegative){
            if (isYNegative){
                if (holoY > playerY){
                    distanceY = holoY - playerY;
                }
                else {
                    distanceY = playerY - holoY;
                }
            }
            else {
                distanceY = playerY - holoY;
            }
        }
        else {
            if (isYNegative){
                distanceY = holoY - playerY;
            }
            else {
                if (holoY > playerY){
                    distanceY = holoY - playerY;
                }
                else {
                    distanceY = playerY - holoY;
                }
            }
        }

        if (isHoloZNegative){
            if (isZNegative){
                if (holoZ > playerZ){
                    distanceZ = holoZ - playerZ;
                }
                else {
                    distanceZ = playerZ - holoZ;
                }
            }
            else {
                distanceZ = playerZ - holoZ;
            }
        }
        else {
            if (isZNegative){
                distanceZ = holoZ - playerZ;
            }
            else {
                if (holoZ > playerZ){
                    distanceZ = holoZ - playerZ;
                }
                else {
                    distanceZ = playerZ - holoZ;
                }
            }
        }
        double distance3D = distanceX + distanceY + distanceZ;

        return distance3D < 65;
    }

    public Map<UUID, HoloPlayer> getPlayerList(){
        return playerList;
    }

    public Map<UUID, HuskiHologram> getHologramList(){
        return hologramList;
    }
}
