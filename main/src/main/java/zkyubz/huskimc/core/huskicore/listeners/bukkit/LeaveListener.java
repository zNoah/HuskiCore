package zkyubz.huskimc.core.huskicore.listeners.bukkit;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.gui.GuiManager;
import zkyubz.huskimc.core.huskicore.managers.holograms.HologramManager;
import zkyubz.huskimc.core.huskicore.managers.player.PlayerTrackerManager;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.ScoreboardMgr;

public class LeaveListener implements Listener {
    private final ScoreboardMgr scoreMgr;
    private final GuiManager guiManager;
    private final PlayerTrackerManager trackerMgr;
    private final HologramManager hologramMgr;

    public LeaveListener(HCore hCore){
        this.scoreMgr = hCore.getScoreboardManager();
        this.guiManager = hCore.getGuiManager();
        this.trackerMgr = hCore.getPlayerTrackerManager();
        this.hologramMgr = hCore.getHologramManager();
    }

    @EventHandler()
    public void onLeave(PlayerQuitEvent event){
        Player player = event.getPlayer();

        scoreMgr.removePlayer(player);
        guiManager.closedWindow(player);
        trackerMgr.removePlayer(player);
        hologramMgr.removePlayer(player);
    }
}
