package zkyubz.huskimc.core.huskicore.managers.scoreboard.variables;

import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.ScoreboardFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.WrappedScoreObjective;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.WrappedScoreTeam;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;
import zkyubz.huskimc.core.huskicore.utils.placeholderapi.PlaceholderUtil;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class HuskiScore {
    private final int scoreId;
    private final HashMap<UUID, ScorePlayer> playerList;

    //Create packets
    private final WrappedPacket createTitlePacketWrap;
    private final WrappedPacket displayPacketWrap;
    private WrappedPacket[] linePacketArrayWrap;
    private WrappedPacket[] createTeamPacketWrapArray;
    private final WrappedPacket[] linkTeamPacketWrapArray;

    //Update packets
    private final WrappedScoreObjective updateTitlePacketWrap;
    private WrappedScoreTeam[] updateTeamPacketWrapArray;

    //Raw text for score
    private String title;
    private String[] lines;

    //Needed to remove lines
    private final Object scoreObjective;

    public HuskiScore(int scoreId, String title, String[] lines){
        ScoreboardFactory factory = new ScoreboardFactory();

        //Creating packets
        this.createTitlePacketWrap = factory.buildTitlePacket(PlaceholderUtil.papiClear(title), scoreId);
        this.updateTitlePacketWrap = (WrappedScoreObjective) factory.buildTitlePacket(title, scoreId);
        this.displayPacketWrap = factory.buildDisplayPacket();

        //Initializing variables for dynamic packets
        this.linePacketArrayWrap = new WrappedPacket[lines.length];
        this.createTeamPacketWrapArray = new WrappedPacket[lines.length];
        this.linkTeamPacketWrapArray = new WrappedPacket[lines.length];
        this.updateTeamPacketWrapArray = new WrappedScoreTeam[lines.length];

        //Creating dynamic packets
        for (int i = 0; i < lines.length; i++){
            this.linePacketArrayWrap[i] = factory.buildLinePacket(i);
            this.createTeamPacketWrapArray[i] = factory.buildTeamPacket(PlaceholderUtil.papiClear(lines[i]), i);
            this.linkTeamPacketWrapArray[i] = factory.buildLinkTeamPacket(i);
            this.updateTeamPacketWrapArray[i] = (WrappedScoreTeam) factory.buildTeamPacket(lines[i], i);
        }

        this.scoreId = scoreId;
        this.playerList = new HashMap<>();
        this.scoreObjective = factory.getScoreObjective();

        //Saving raw text
        this.title = title;
        this.lines = lines;
    }

    public void initPlayer(ScorePlayer scorePlayer){
        Player player = scorePlayer.getPlayer();
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        //Create title/scoreboard
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(createTitlePacketWrap.getPacket());

        //Update title
        String title = PlaceholderUtil.papiParse(player, this.title);
        updateTitlePacketWrap.update(title);
        nmsPlayer.sendPacket(updateTitlePacketWrap.getPacket());

        //Send lines
        for (int i = 0; i < updateTeamPacketWrapArray.length; i++){
            //Create Line and Team
            nmsPlayer.sendPacket(linePacketArrayWrap[i].getPacket());
            nmsPlayer.sendPacket(createTeamPacketWrapArray[i].getPacket());
            nmsPlayer.sendPacket(linkTeamPacketWrapArray[i].getPacket());

            //Update team (Line text)
            String line = PlaceholderUtil.papiParse(player, lines[i]);
            updateTeamPacketWrapArray[i].update(line);
            nmsPlayer.sendPacket(updateTeamPacketWrapArray[i].getPacket());
        }

        //Display scoreboard
        nmsPlayer.sendPacket(displayPacketWrap.getPacket());

        playerList.put(player.getUniqueId(), scorePlayer);
    }

    public void removePlayer(ScorePlayer scorePlayer){
        Player player = scorePlayer.getPlayer();
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        playerList.remove(player.getUniqueId());

        ScoreboardFactory factory = new ScoreboardFactory(this.scoreObjective);
        WrappedPacket removeScorePacketWrap = factory.buildRemoveScorePacket(scoreId);

        assert nmsPlayer != null;
        nmsPlayer.sendPacket(removeScorePacketWrap.getPacket());
    }

    public void showScorePlayer(ScorePlayer scorePlayer){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(scorePlayer.getPlayer());
        playerList.put(scorePlayer.getPlayer().getUniqueId(), scorePlayer);

        //Display scoreboard
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(displayPacketWrap.getPacket());
    }

    public void updatePlayerTitle(Player player){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        //Update title (Duh)
        String title = PlaceholderUtil.papiParse(player, this.title);
        updateTitlePacketWrap.update(title);
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(updateTitlePacketWrap.getPacket());
    }

    public void updatePlayerLine(Player player, int index){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        //Update team (Line text) (Duh)
        String line = PlaceholderUtil.papiParse(player, lines[index]);
        updateTeamPacketWrapArray[index].update(line);
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(updateTeamPacketWrapArray[index].getPacket());
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setLine(int index, String line){
        this.lines[index] = line;
    }

    //Todo: Make a better implementation of this
    public void newLines(String[] lines, List<Player> playerList){
        int oldLineAmount = this.lines.length;
        int newLineAmount = lines.length;

        WrappedPacket[] linePacketWrapArray = new WrappedPacket[newLineAmount];
        WrappedPacket[] createTeamPacketWrapArray = new WrappedPacket[newLineAmount];
        WrappedPacket[] linkTeamPacketWrapArray = new WrappedPacket[newLineAmount];
        WrappedScoreTeam[] updateTeamPacketWrapArray = new WrappedScoreTeam[newLineAmount];

        WrappedPacket[] deleteLinePacketWrapArray = new WrappedPacket[newLineAmount];
        WrappedPacket[] deleteTeamPacketWrapArray = new WrappedPacket[newLineAmount];

        ScoreboardFactory factory = new ScoreboardFactory(this.scoreObjective);

        for (int i = 0; i < oldLineAmount; i++){
            deleteLinePacketWrapArray[i] = factory.buildRemoveLinePacket(i);
            deleteTeamPacketWrapArray[i] = factory.buildRemoveTeamPacket(i);
        }

        for (int i = 0; i < newLineAmount; i++){
            linePacketWrapArray[i] = factory.buildLinePacket(i);
            createTeamPacketWrapArray[i] = factory.buildTeamPacket(lines[i], i);
            linkTeamPacketWrapArray[i] = factory.buildLinkTeamPacket(i);
            updateTeamPacketWrapArray[i] = (WrappedScoreTeam) factory.buildTeamPacket(lines[i], i);
        }

        for (Player player : playerList){
            NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

            for (int i = 0; i < oldLineAmount; i++){
                assert nmsPlayer != null;
                nmsPlayer.sendPacket(deleteLinePacketWrapArray[i].getPacket());
                nmsPlayer.sendPacket(deleteTeamPacketWrapArray[i].getPacket());
            }

            for (int i = 0; i < newLineAmount; i++){
                assert nmsPlayer != null;
                nmsPlayer.sendPacket(linePacketWrapArray[i].getPacket());
                nmsPlayer.sendPacket(createTeamPacketWrapArray[i].getPacket());
                nmsPlayer.sendPacket(linkTeamPacketWrapArray[i].getPacket());

                //Update team (Line text)
                String line = PlaceholderUtil.papiParse(player, lines[i]);
                updateTeamPacketWrapArray[i].update(line);
                nmsPlayer.sendPacket(updateTeamPacketWrapArray[i].getPacket());
            }
        }

        this.linePacketArrayWrap = linePacketWrapArray;
        this.createTeamPacketWrapArray = createTeamPacketWrapArray;
        this.updateTeamPacketWrapArray = updateTeamPacketWrapArray;

        this.lines = lines;
    }

    public HashMap<UUID, ScorePlayer> getPlayerList(){
        return playerList;
    }
}
