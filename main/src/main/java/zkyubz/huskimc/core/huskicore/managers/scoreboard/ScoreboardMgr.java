package zkyubz.huskimc.core.huskicore.managers.scoreboard;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.tasks.LineUpdateTask;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.tasks.TitleUpdateTask;
import zkyubz.huskimc.core.huskicore.managers.variables.AnimText;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.variables.HuskiScore;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.variables.ScorePlayer;

import java.util.*;

public class ScoreboardMgr {
    private final Map<Integer, HuskiScore> scoreList;                                 //Integer scoreId, HuskiScore score
    private final Map<Integer, Integer> titleUpdateTaskIdList;                        //Integer scoreId, Integer taskId
    private final Map<Integer, List<Integer>> lineUpdateTaskIdList;                   //Integer scoreId, List<Integer> taskIdList
    private final Map<UUID, ScorePlayer> playerList;

    private int lastId = -1;

    public ScoreboardMgr(){
        this.scoreList = new HashMap<>();
        this.titleUpdateTaskIdList = new HashMap<>();
        this.lineUpdateTaskIdList = new HashMap<>();
        this.playerList = new HashMap<>();
    }

    //Returns the scoreboard id when created
    public int addScoreboard(String title, String[] lines){
        int id = ++this.lastId;
        HuskiScore hScore = new HuskiScore(id, title, lines);

        scoreList.put(id, hScore);

        return id;
    }

    //Return true if removed, false if not found
    public boolean removeScoreboard(int scoreId){
        HuskiScore hScore = scoreList.get(scoreId);

        for (ScorePlayer sPlayer : playerList.values()) {
            if (sPlayer.getScoreId() == scoreId){
                hScore.removePlayer(sPlayer);
                sPlayer.setScoreId(-1);
            }
        }

        return scoreList.remove(scoreId) != null;
    }

    //Todo: Consider returning enum instead
    //Return true if removed, false if error
    public boolean removeScoreboard(int scoreId, int scoreFallback){
        HuskiScore hScoreRemove = scoreList.get(scoreId);
        HuskiScore hScoreFallback = scoreList.get(scoreFallback);

        if (hScoreRemove != null && hScoreFallback != null){
            for (ScorePlayer sPlayer : playerList.values()) {
                if (sPlayer.getScoreId() == scoreId){
                    hScoreRemove.removePlayer(sPlayer);
                    hScoreFallback.initPlayer(sPlayer);

                    sPlayer.setScoreId(scoreFallback);
                }
            }
        }

        return scoreList.remove(scoreId) != null;
    }

    public void animateScore(int scoreId, AnimText titleAnim, HashMap<Integer, AnimText> animLines){
        HuskiScore hScore = scoreList.get(scoreId);

        if (hScore != null){
            String[] titleList = titleAnim.getTextArray();

            TitleUpdateTask titleUpdateTask = new TitleUpdateTask(hScore, titleList);

            BukkitTask titleBukkitTask = titleUpdateTask.runTaskTimerAsynchronously(HCore.getInstance(), 0, titleAnim.getDelay());
            titleUpdateTaskIdList.put(scoreId, titleBukkitTask.getTaskId());

            List<Integer> lineTaskList = new ArrayList<>();
            for (Map.Entry<Integer, AnimText> mapEntry : animLines.entrySet()){
                AnimText animLine = mapEntry.getValue();
                String[] lineList = animLine.getTextArray();
                int lineNumber = mapEntry.getKey();

                LineUpdateTask lineUpdateTask = new LineUpdateTask(hScore, lineList, lineNumber);

                BukkitTask lineBukkitTask = lineUpdateTask.runTaskTimerAsynchronously(HCore.getInstance(), 0, animLine.getDelay());
                lineTaskList.add(lineBukkitTask.getTaskId());
            }

            lineUpdateTaskIdList.put(scoreId, lineTaskList);
        }
    }

    //Todo: Consider returning true/false
    public void addPlayer(Player player){
        addPlayer(player, 0); //0 = Default scoreboard
    }

    //Todo: Consider returning true/false
    public void addPlayer(Player player, int scoreId){
        HuskiScore hScore = scoreList.get(scoreId);
        ScorePlayer sPlayer;

        if (hScore != null){
            sPlayer = new ScorePlayer(player, scoreId);
            hScore.initPlayer(sPlayer);
        }
        else {
            sPlayer = new ScorePlayer(player, -1);
        }

        playerList.put(player.getUniqueId(), sPlayer);
    }

    public void removePlayer(Player player){
        ScorePlayer sPlayer = playerList.get(player.getUniqueId());

        if (sPlayer != null){
            HuskiScore hScore = scoreList.get(sPlayer.getScoreId());

            if (hScore != null){
                hScore.removePlayer(sPlayer);
            }

            playerList.remove(player.getUniqueId());
        }
    }

    public void switchScoreboard(Player player, int scoreId){
        HuskiScore hScore = scoreList.get(scoreId);

        if (hScore != null){
            ScorePlayer sPlayer = playerList.get(player.getUniqueId());
            HuskiScore hScoreOld = scoreList.get(sPlayer.getScoreId());

            if (hScoreOld != null){
                hScoreOld.removePlayer(sPlayer);
            }

            hScore.initPlayer(sPlayer);
            sPlayer.setScoreId(scoreId);
        }
    }

    public HuskiScore getHuskiScore(int scoreId){
        return scoreList.get(scoreId);
    }
}
