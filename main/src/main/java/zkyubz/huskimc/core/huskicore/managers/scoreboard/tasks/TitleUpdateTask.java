package zkyubz.huskimc.core.huskicore.managers.scoreboard.tasks;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.variables.HuskiScore;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.variables.ScorePlayer;

import java.util.Collection;

public class TitleUpdateTask extends BukkitRunnable {
    private final HuskiScore huskiScore;
    private final String[] titles;

    private int lastIndex;

    public TitleUpdateTask(HuskiScore huskiScore, String[] titles){
        this.huskiScore = huskiScore;
        this.titles = titles;
        this.lastIndex = 0;
    }

    @Override
    public void run() {
        Collection<ScorePlayer> scorePlayerList = huskiScore.getPlayerList().values();

        if (lastIndex == titles.length){
            lastIndex = 0;
        }

        huskiScore.setTitle(titles[lastIndex]);

        for (ScorePlayer sPlayer : scorePlayerList){
            Player player = sPlayer.getPlayer();
            huskiScore.updatePlayerTitle(player);
        }

        lastIndex++;
    }
}
