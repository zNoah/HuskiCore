package zkyubz.huskimc.core.huskicore.managers.holograms.variables;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class HoloPlayer {
    private final List<UUID> hologramVisible;
    private final Player player;

    public HoloPlayer(Player player){
        this.hologramVisible = new ArrayList<>();
        this.player = player;
    }

    public void addHologramVisible(UUID holoId){
        hologramVisible.add(holoId);
    }

    public void removeHologramVisible(UUID holoId){
        hologramVisible.remove(holoId);
    }

    public boolean containsHologram(UUID holoId){
        for (int i = 0; i < hologramVisible.size(); i++){
            if (hologramVisible.get(i) == holoId){
                return true;
            }
        }
        return false;
    }

    public List<UUID> getHologramVisible(){
        return hologramVisible;
    }

    public Player getPlayer() {
        return player;
    }
}
