package zkyubz.huskimc.core.huskicore.managers.player;

import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.player.variables.PlayerTracker;
import zkyubz.huskimc.core.huskicore.managers.player.variables.PlayerTrackerRunnable;

import java.util.*;

public class PlayerTrackerManager {
    private final List<PlayerTrackerRunnable> runnableList;
    private final Map<UUID, PlayerTracker> trackerList;

    public PlayerTrackerManager(){
        this.runnableList = new ArrayList<>();
        this.trackerList = new HashMap<>();
    }

    public void addPlayer(Player player){
        PlayerTracker playerTracker = new PlayerTracker(this, player);
        playerTracker.runTaskTimerAsynchronously(HCore.getInstance(), 1, 1);

        trackerList.put(player.getUniqueId(), playerTracker);
    }

    public void removePlayer(Player player){
        PlayerTracker playerTracker = trackerList.get(player.getUniqueId());
        if (playerTracker != null){
            playerTracker.cancel();
            trackerList.remove(player.getUniqueId());
        }
    }

    public void addRunnable(PlayerTrackerRunnable playerTrackerRunnable){
        runnableList.add(playerTrackerRunnable);
    }

    public void removeRunnable(PlayerTrackerRunnable playerTrackerRunnable){
        runnableList.remove(playerTrackerRunnable);
    }

    public List<PlayerTrackerRunnable> getRunnableList() {
        return runnableList;
    }
}
