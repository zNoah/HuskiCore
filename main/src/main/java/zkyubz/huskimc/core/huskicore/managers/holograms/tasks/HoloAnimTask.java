package zkyubz.huskimc.core.huskicore.managers.holograms.tasks;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zkyubz.huskimc.core.huskicore.managers.holograms.HologramManager;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HoloPlayer;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HuskiHologram;
import zkyubz.huskimc.core.huskicore.managers.variables.AnimText;

import java.util.Collection;
import java.util.Map;

public class HoloAnimTask extends BukkitRunnable {
    private final HologramManager hologramManager;
    private final HuskiHologram huskiHologram;
    private final Map<Integer, AnimText> animLines;

    private int internalTick;
    private final int maxTick;

    public HoloAnimTask(HologramManager hologramManager, HuskiHologram huskiHologram, Map<Integer, AnimText> animLines){
        this.hologramManager = hologramManager;
        this.huskiHologram = huskiHologram;
        this.animLines = animLines;

        int maxTick = 1;
        for (Map.Entry<Integer, AnimText> mapEntry : animLines.entrySet()){
            //Get maxTick
            int delay = mapEntry.getValue().getDelay();
            if (delay > maxTick){
                maxTick = delay;
            }

            //Initialize animScore
            int lineNumber = mapEntry.getKey();
            AnimText animText = mapEntry.getValue();
            updateLine(lineNumber, animText);
        }

        this.maxTick = maxTick;
        this.internalTick = 1;
    }

    @Override
    public void run() {
        if (internalTick > maxTick){
            internalTick = 1;
        }

        for (Map.Entry<Integer, AnimText> animEntry : animLines.entrySet()){
            int lineNumber = animEntry.getKey();
            AnimText animText = animEntry.getValue();
            int delay = animText.getDelay();

            if (internalTick % delay == 0){
                updateLine(lineNumber, animText);
            }
        }

        internalTick++;
    }

    private void updateLine(int lineNumber, AnimText animText){
        int nextIndex = animText.nextIndex();
        String textToUse = animText.getTextArray()[nextIndex];
        huskiHologram.setLine(lineNumber, textToUse);

        Collection<HoloPlayer> holoPlayerList = hologramManager.getPlayerList().values();
        for (HoloPlayer holoPlayer : holoPlayerList){
            Player player = holoPlayer.getPlayer();
            huskiHologram.playerUpdateLine(player, lineNumber);
        }
    }
}
