package zkyubz.huskimc.core.huskicore.managers.variables;

import java.util.List;

public class AnimText {
    private final List<String> textList;
    private final int delay;

    private int currentIndex;

    public AnimText (List<String> textList, int delay){
        this.textList = textList;
        this.delay = delay;
        this.currentIndex = 0;
    }

    public List<String> getTextList(){
        return textList;
    }

    public String[] getTextArray(){
        return textList.toArray(new String[0]);
    }

    public int getDelay(){
        return delay;
    }

    public int getCurrentIndex(){
        return currentIndex;
    }

    public int nextIndex(){
        currentIndex++;
        if (currentIndex == textList.size()){
            currentIndex = 0;
        }
        return currentIndex;
    }
}
