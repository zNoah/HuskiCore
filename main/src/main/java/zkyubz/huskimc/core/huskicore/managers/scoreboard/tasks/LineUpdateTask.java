package zkyubz.huskimc.core.huskicore.managers.scoreboard.tasks;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.variables.HuskiScore;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.variables.ScorePlayer;

import java.util.Collection;

public class LineUpdateTask extends BukkitRunnable {
    private final HuskiScore huskiScore;
    private final String[] lines;

    private final int lineNumber;
    private int lastIndex;

    public LineUpdateTask(HuskiScore huskiScore, String[] lines, int lineNumber){
        this.huskiScore = huskiScore;
        this.lines = lines;
        this.lineNumber = lineNumber;
        this.lastIndex = 0;
    }

    @Override
    public void run() {
        Collection<ScorePlayer> scorePlayerList = huskiScore.getPlayerList().values();

        if (lastIndex == lines.length){
            this.lastIndex = 0;
        }

        huskiScore.setLine(lineNumber, lines[lastIndex]);

        for (ScorePlayer sPlayer : scorePlayerList){
            Player player = sPlayer.getPlayer();
            huskiScore.updatePlayerLine(player, lineNumber);
        }

        this.lastIndex++;
    }
}
