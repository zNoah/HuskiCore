package zkyubz.huskimc.core.huskicore.managers;

import zkyubz.huskimc.core.huskicore.utils.variables.PacketInRunnable;
import zkyubz.huskimc.core.huskicore.utils.variables.PacketOutRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NmsPacketManager {
    private final List<PacketInRunnable> inPacketRunnableList;
    private final List<PacketOutRunnable> outPacketRunnableList;

    public NmsPacketManager(){
        this.inPacketRunnableList = new ArrayList<>();
        this.outPacketRunnableList = new ArrayList<>();
    }

    public void addInPacketRunnable(PacketInRunnable packetRunnable){
        inPacketRunnableList.add(packetRunnable);
    }

    public void addOutPacketRunnable(PacketOutRunnable packetRunnable){
        outPacketRunnableList.add(packetRunnable);
    }

    public boolean removeInPacketRunnable(PacketInRunnable packetRunnable){
        return inPacketRunnableList.remove(packetRunnable);
    }

    public boolean removeOutPacketRunnable(PacketOutRunnable packetRunnable){
        return outPacketRunnableList.remove(packetRunnable);
    }

    public boolean removeInPacketRunnable(int id){
        return inPacketRunnableList.remove(id) != null;
    }

    public boolean removeOutPacketRunnable(int id){
        return outPacketRunnableList.remove(id) != null;
    }

    public List<PacketInRunnable> getInPacketRunnableList(){
        return inPacketRunnableList;
    }

    public List<PacketOutRunnable> getOutPacketRunnableList(){
        return outPacketRunnableList;
    }
}
