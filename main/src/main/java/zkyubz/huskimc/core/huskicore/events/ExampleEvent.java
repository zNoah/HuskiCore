package zkyubz.huskimc.core.huskicore.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ExampleEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    public ExampleEvent(){

    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}

