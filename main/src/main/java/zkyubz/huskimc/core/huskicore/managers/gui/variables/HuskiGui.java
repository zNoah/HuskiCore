package zkyubz.huskimc.core.huskicore.managers.gui.variables;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WindowFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WrappedWindowItems;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;
import zkyubz.huskimc.core.huskicore.utils.placeholderapi.PlaceholderUtil;

import java.util.ArrayList;
import java.util.List;

public class HuskiGui {
    private final WrappedPacket wrappedOpenWindow;
    private final WrappedPacket wrappedCloseWindow;
    private final WindowFactory factory;
    private final List<ItemStack> itemStackList;

    public HuskiGui(int windowId, String title, List<ItemStack> itemStackList){
        WindowFactory factory = new WindowFactory();

        this.wrappedOpenWindow = factory.buildOpenWindowPacket(windowId, title, itemStackList.size()); //???
        this.wrappedCloseWindow = factory.buildCloseWindowPacket();
        this.factory = factory;
        this.itemStackList = itemStackList;
    }

    public void openWindow(Player player){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        assert nmsPlayer != null;
        nmsPlayer.sendPacket(wrappedOpenWindow.getPacket());
        WrappedPacket wrappedWindowItems = createWrappedWindowItems(player);
        nmsPlayer.sendPacket(wrappedWindowItems.getPacket());
    }

    public void reloadWindow(Player player){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        WrappedPacket wrappedWindowItems = createWrappedWindowItems(player);
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(wrappedWindowItems.getPacket());
    }

    public void closeWindow(Player player){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(wrappedCloseWindow.getPacket());
    }

    public WrappedWindowItems createWrappedWindowItems(Player player){
        List<ItemStack> itemStackList = new ArrayList<>();
        for (int i = 0; i < this.itemStackList.size(); i++){
            ItemStack bukkitItemStack = this.itemStackList.get(i);

            if (bukkitItemStack != null){
                bukkitItemStack = this.itemStackList.get(i).clone();
                ItemMeta itemMeta = bukkitItemStack.getItemMeta();
                itemMeta.setDisplayName(PlaceholderUtil.papiParse(player, itemMeta.getDisplayName()));

                List<String> loreList = itemMeta.getLore();
                for (int j = 0; j < loreList.size(); j++){
                    String lore = loreList.get(j);
                    lore = PlaceholderUtil.papiParse(player, lore);
                    loreList.set(j, lore);
                }

                bukkitItemStack.setItemMeta(itemMeta);
            }
            itemStackList.add(bukkitItemStack);
        }
        return factory.buildWindowItemsPacket(itemStackList, player);
    }

    public WrappedPacket getWrappedOpenWindow(){
        return wrappedOpenWindow;
    }

    public WrappedPacket getWrappedCloseWindow(){
        return wrappedCloseWindow;
    }
}
