package zkyubz.huskimc.core.huskicore.managers.player.variables;

import org.bukkit.entity.Player;

public interface PlayerTrackerRunnable {
    void run(Player player, double x, double y, double z);
}
