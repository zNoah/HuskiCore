package zkyubz.huskimc.core.huskicore.events.packets;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import zkyubz.huskimc.core.huskicore.managers.holograms.variables.HuskiHologram;

import java.util.UUID;

public class HoloClickEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final int entityId;
    private final HuskiHologram huskiHologram;
    private final Player player;

    public HoloClickEvent(Player player, HuskiHologram huskiHologram, int entityId){
        this.player = player;
        this.entityId = entityId;
        this.huskiHologram = huskiHologram;
    }

    public Player getPlayer() {
        return player;
    }

    public int getEntityId() {
        return entityId;
    }

    public HuskiHologram getHologram() {
        return huskiHologram;
    }

    public UUID getHoloUUID(){
        return huskiHologram.getHoloUUID();
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
