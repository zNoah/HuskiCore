package zkyubz.huskimc.core.huskicore.managers.scoreboard.variables;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ScorePlayer {
    private final Player player;
    private int scoreId;

    public ScorePlayer(Player player, int scoreId){
        this.player = player;
        this.scoreId = scoreId;
    }

    public Player getPlayer(){
        return player;
    }

    public int getScoreId(){
        return scoreId;
    }

    public void setScoreId(int scoreId){
        this.scoreId = scoreId;
    }
}
