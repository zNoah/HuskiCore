package zkyubz.huskimc.core.huskicore;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import zkyubz.huskimc.core.huskicore.managers.NmsPacketManager;
import zkyubz.huskimc.core.huskicore.managers.gui.GuiManager;
import zkyubz.huskimc.core.huskicore.managers.holograms.HologramManager;
import zkyubz.huskimc.core.huskicore.managers.player.PlayerTrackerManager;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.ScoreboardMgr;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;

public class HPlugin extends JavaPlugin {
    private ScoreboardMgr scoreboardMgr;
    private GuiManager guiManager;
    private PlayerTrackerManager playerTrackerManager;
    private HologramManager hologramMgr;
    private NmsPacketManager nmsPacketManager;
    private static NMSVersion nmsVersion;

    //Set and get for the managers
    void setScoreboardMgr(ScoreboardMgr scoreboardMgr){
        this.scoreboardMgr = scoreboardMgr;
    }

    public ScoreboardMgr getScoreboardManager(){
        return scoreboardMgr;
    }

    void setGuiManager(GuiManager guiManager){
        this.guiManager = guiManager;
    }

    public GuiManager getGuiManager(){
        return guiManager;
    }

    void setPacketManager(NmsPacketManager nmsPacketManager){
        this.nmsPacketManager = nmsPacketManager;
    }

    public NmsPacketManager getPacketManager(){
        return nmsPacketManager;
    }

    void setPlayerTrackerManager(PlayerTrackerManager playerTrackerManager){
        this.playerTrackerManager = playerTrackerManager;
    }

    public PlayerTrackerManager getPlayerTrackerManager(){
        return playerTrackerManager;
    }

    void setHologramMgr(HologramManager hologramMgr){
        this.hologramMgr = hologramMgr;
    }

    public HologramManager getHologramManager(){
        return hologramMgr;
    }

    //Set and get nms version
    void setVersion(){
        String nmsVersion = Bukkit.getServer().getClass().getPackage().getName();
        nmsVersion = nmsVersion.substring(nmsVersion.lastIndexOf('.') + 1);

        switch (nmsVersion){
            case "v1_8_R3":
                HPlugin.nmsVersion = NMSVersion.v1_8_R3;
                break;
            //case "v1_12_R2":
            //    HPlugin.nmsVersion = NMSVersion.v1_12_R1;
            //    break;
            //case "v1_17_R1":
            //    HPlugin.nmsVersion = NMSVersion.v1_17_R1;
            //    break;
            default:
                HPlugin.nmsVersion = NMSVersion.UNSUPPORTED;
        }

        NMSUtil.setNMSVersion(getNMSVersion());
    }

    public static NMSVersion getNMSVersion(){
        return nmsVersion;
    }

}
