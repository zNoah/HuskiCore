package zkyubz.huskimc.core.huskicore.managers.holograms.variables;

import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.NmsPacketManager;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedSpawnEntity;

import java.util.ArrayList;
import java.util.List;

public class HoloButton extends HuskiHologram {
    private final List<HoloButtonAction> actionList;
    private final HuskiHologram huskiHologram;

    private HoloClickRunnable holoClickRunnable;

    public HoloButton(HuskiHologram huskiHologram){
        super(huskiHologram.getWorldUUID(), huskiHologram.getX(), huskiHologram.getY() - 1.45, huskiHologram.getZ(), -1.75, true, false);
        addLines(huskiHologram.getLineAmount());
        registerPacketListener();

        this.actionList = new ArrayList<>();
        this.huskiHologram = huskiHologram;
    }

    public HoloButton(HuskiHologram huskiHologram, List<HoloButtonAction> actionList){
        super(huskiHologram.getWorldUUID(), huskiHologram.getX(), huskiHologram.getY() - 1.5, huskiHologram.getZ(), -1.75, true, false);
        addLines(huskiHologram.getLineAmount());
        registerPacketListener();

        this.actionList = actionList;
        this.huskiHologram = huskiHologram;
    }

    public void addAction(HoloButtonAction holoButtonAction){
        actionList.add(holoButtonAction);
    }

    public void removeAction(HoloButtonAction holoButtonAction){
        actionList.remove(holoButtonAction);
    }

    private void addLines(int originalAmount){
        List<String> lineList = new ArrayList<>();
        int amount = (int) Math.ceil((double) originalAmount / 7);

        for (int i = 0; i < amount; i++){
            lineList.add("");
        }

        replaceAll(lineList);
    }

    private void registerPacketListener(){
        NmsPacketManager nmsPacketManager = HCore.getInstance().getPacketManager();
        HoloClickRunnable holoClickRunnable = new HoloClickRunnable(this);

        nmsPacketManager.addInPacketRunnable(holoClickRunnable);

        this.holoClickRunnable = holoClickRunnable;
    }

    public void unregisterPacketListener(){
        NmsPacketManager nmsPacketManager = HCore.getInstance().getPacketManager();

        nmsPacketManager.removeInPacketRunnable(holoClickRunnable);

        this.holoClickRunnable = null;
    }

    public List<Integer> getEntityIdList(){
        List<Integer> idList = new ArrayList<>();

        for (int i = 0; i < getWrappedSpawnEntities().size(); i++){
            WrappedSpawnEntity wrappedPacket = (WrappedSpawnEntity) getWrappedSpawnEntities().get(i);
            idList.add(wrappedPacket.getEntityID());
        }

        return idList;
    }

    public HuskiHologram getHuskiHologram(){
        return huskiHologram;
    }

    public List<HoloButtonAction> getActionList(){
        return actionList;
    }
}
