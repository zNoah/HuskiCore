package zkyubz.huskimc.core.huskicore.events.packets;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class ItemClickEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final int guiId;
    private final int slot;
    private final Player player;
    private final int buttonPressed;
    private final ItemStack itemStack;

    public ItemClickEvent(Player player, int guiId, int slot, int buttonPressed, ItemStack itemStack){
        this.guiId = guiId;
        this.slot = slot;
        this.player = player;
        this.buttonPressed = buttonPressed;
        this.itemStack = itemStack;
    }

    public int getGuiId(){
        return guiId;
    }

    public int getSlot(){
        return slot;
    }

    public Player getPlayer(){
        return player;
    }

    public int getButtonPressed(){
        return buttonPressed;
    }

    public ItemStack getItemStack(){
        return itemStack;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
