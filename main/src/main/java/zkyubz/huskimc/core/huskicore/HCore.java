package zkyubz.huskimc.core.huskicore;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;

import zkyubz.huskimc.core.huskicore.commands.MainCommand;
import zkyubz.huskimc.core.huskicore.events.huskicore.HCoreLoadedEvent;
import zkyubz.huskimc.core.huskicore.listeners.bukkit.JoinListener;
import zkyubz.huskimc.core.huskicore.listeners.bukkit.LeaveListener;
import zkyubz.huskimc.core.huskicore.managers.NmsPacketManager;
import zkyubz.huskimc.core.huskicore.managers.gui.GuiManager;
import zkyubz.huskimc.core.huskicore.managers.holograms.HologramManager;
import zkyubz.huskimc.core.huskicore.managers.player.PlayerTrackerManager;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.ScoreboardMgr;
import zkyubz.huskimc.core.huskicore.utils.ChatColor;
import zkyubz.huskimc.core.huskicore.utils.ColorUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.placeholderapi.PlaceholderUtil;

public class HCore extends HPlugin {
    private static HCore hCore;

    @Override
    public void onEnable(){
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        PluginManager pluginManager = getServer().getPluginManager();

        hCore = this;
        setVersion();
        ColorUtil.setValues();

        console.sendMessage(" ");
        console.sendMessage(ChatColor.format("       &b&lHuskiCore " + this.getDescription().getVersion() + " "));
        console.sendMessage(" ");
        console.sendMessage(ChatColor.format(" ➜ &fCreado por &bNua#1962"));
        console.sendMessage(ChatColor.format(" ➜ &fPara: &aHuskiMC"));
        console.sendMessage(" ");

        //Checking if server version is supported
        if (getNMSVersion() == NMSVersion.UNSUPPORTED){
            throw new UnsupportedOperationException();
        }

        //Set if PlaceholderAPI is present
        PlaceholderUtil.setUsePapi(pluginManager.isPluginEnabled("PlaceholderAPI"));

        //Load commands
        this.getCommand("huskicore").setExecutor(new MainCommand());


        //Loading managers
        ScoreboardMgr scoreboardMgr = new ScoreboardMgr();
        setScoreboardMgr(scoreboardMgr);

        NmsPacketManager nmsPacketManager = new NmsPacketManager();
        setPacketManager(nmsPacketManager);

        GuiManager guiManager = new GuiManager();
        setGuiManager(guiManager);

        PlayerTrackerManager playerTrackerManager = new PlayerTrackerManager();
        setPlayerTrackerManager(playerTrackerManager);

        HologramManager hologramManager = new HologramManager(playerTrackerManager);
        setHologramMgr(hologramManager);

        //Loading listeners
        pluginManager.registerEvents(new JoinListener(this), this);
        pluginManager.registerEvents(new LeaveListener(this), this);

        //Plugin loaded
        HCoreLoadedEvent loadedEvent = new HCoreLoadedEvent();
        pluginManager.callEvent(loadedEvent);
    }

    public static HCore getInstance(){
        return hCore;
    }
}
