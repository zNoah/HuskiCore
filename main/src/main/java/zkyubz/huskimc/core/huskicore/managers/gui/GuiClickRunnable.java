package zkyubz.huskimc.core.huskicore.managers.gui;

import io.netty.channel.ChannelHandlerContext;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import zkyubz.huskimc.core.huskicore.events.packets.ItemClickEvent;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WindowFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WrappedWindowClick;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.variables.PacketInRunnable;

public class GuiClickRunnable implements PacketInRunnable {
    private final GuiManager guiManager;

    public GuiClickRunnable(GuiManager guiManager){
        this.guiManager = guiManager;
    }

    @Override
    public void execute(Player player, ChannelHandlerContext channelHandlerContext, WrappedPacket wrappedPacket) {
        if (wrappedPacket.getPacketType() == PacketType.WINDOW_CLICK){
            WrappedWindowClick wrappedWindowClick = (WrappedWindowClick) wrappedPacket;
            int windowId = wrappedWindowClick.getWindowId();

            //Is this a HuskiCore gui?
            if (windowId == 101){
                int playerCurrentGui = guiManager.currentGui(player);
                if (playerCurrentGui != -1){
                    int slot = wrappedWindowClick.getSlot();
                    NMSPlayer nmsPlayer = new NMSPlayer_v1_8_R3(player);
                    WindowFactory factory = new WindowFactory();

                    /*
                     * Reset slot on click (Mode 0)
                     * Reload menu on shift+click (Mode 1)
                     * Reload menu on number (Mode 2)
                     * Reload menu on drop? (Mode 4)
                     * Reload menu on double click (Mode 6)
                     * Don't do anything for middle click (Mode 3)
                     */

                    switch (wrappedWindowClick.getMode()){
                        case 0:
                            WrappedPacket wrappedSetSlot = factory.buildSetSlotPacket(-1, -1, null);
                            nmsPlayer.sendPacket(wrappedSetSlot.getPacket());

                            Object nmsItemStack = wrappedWindowClick.getNmsItemStack();
                            wrappedSetSlot = factory.buildSetSlotPacket(windowId, slot, nmsItemStack);
                            nmsPlayer.sendPacket(wrappedSetSlot.getPacket());
                            break;
                        case 1:
                        case 2:
                        case 4:
                        case 6:
                            guiManager.reloadGui(player, playerCurrentGui);
                            break;
                        default:
                            break;
                    }

                    //Launch event
                    ItemClickEvent event = new ItemClickEvent(player, playerCurrentGui, slot, wrappedWindowClick.getButtonPressed(), wrappedWindowClick.getBukkitItemStack());
                    Bukkit.getServer().getPluginManager().callEvent(event);
                }
            }
        }
    }
}
