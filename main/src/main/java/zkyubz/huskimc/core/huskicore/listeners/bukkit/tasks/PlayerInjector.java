package zkyubz.huskimc.core.huskicore.listeners.bukkit.tasks;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.NmsPacketManager;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketWrapper;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;
import zkyubz.huskimc.core.huskicore.utils.variables.PacketInRunnable;
import zkyubz.huskimc.core.huskicore.utils.variables.PacketOutRunnable;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerInjector extends BukkitRunnable {
    private final NmsPacketManager nmsPacketManager;
    private final Player player;

    public PlayerInjector(Player player, NmsPacketManager nmsPacketManager){
        this.player = player;
        this.nmsPacketManager = nmsPacketManager;
    }

    @Override
    public void run() {
        inject(player);
    }

    private void inject(Player player){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        ChannelDuplexHandler channelDuplexHandler = new ChannelDuplexHandler() {
            @Override
            public void channelRead(ChannelHandlerContext channelHandlerContext, Object packet) throws Exception {
                //Inbound packets
                WrappedPacket wrappedPacket = PacketWrapper.wrapInPacket(packet);

                if (wrappedPacket != null){
                    List<PacketInRunnable> packetInRunnableList = nmsPacketManager.getInPacketRunnableList();

                    for (PacketInRunnable inPacketRunnable : packetInRunnableList){
                        inPacketRunnable.execute(player, channelHandlerContext, wrappedPacket);
                    }
                }

                super.channelRead(channelHandlerContext, packet);
            }

            @Override
            public void write(ChannelHandlerContext channelHandlerContext, Object packet, ChannelPromise channelPromise) throws Exception {
                //Outbound packets
                WrappedPacket wrappedPacket = PacketWrapper.wrapOutPacket(packet);

                if (wrappedPacket != null){
                    List<PacketOutRunnable> packetOutRunnableList = nmsPacketManager.getOutPacketRunnableList();
                    boolean cancel = false;

                    for (PacketOutRunnable outPacketRunnable : packetOutRunnableList){
                        outPacketRunnable.execute(player, channelHandlerContext, wrappedPacket, channelPromise);
                        if (outPacketRunnable.cancelPacket()){
                            cancel = true;
                        }
                    }

                    if (cancel){
                        return;
                    }
                }

                super.write(channelHandlerContext, packet, channelPromise);
            }
        };

        //You inject the player here
        try {
            assert nmsPlayer != null;
            ChannelPipeline pipeline = (ChannelPipeline) nmsPlayer.getPipeline();
            pipeline.addBefore("packet_handler", "hcore_packetlistener", channelDuplexHandler);
        }
        catch (NoSuchElementException exception){
            Logger logger = HCore.getInstance().getLogger();
            logger.log(Level.WARNING, "Player " + player.getName() + " couldn't get injected");
            logger.log(Level.WARNING, "Reason: NoSuchElementException (packet_handler)");
            logger.log(Level.WARNING, "Is player online?: " + player.isOnline());
        }
    }
}
