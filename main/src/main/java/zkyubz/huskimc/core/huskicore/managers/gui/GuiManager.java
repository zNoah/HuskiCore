package zkyubz.huskimc.core.huskicore.managers.gui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.gui.variables.HuskiGui;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class GuiManager {
    private final HashMap<Integer, HuskiGui> guiList;                     //Integer id, HuskiGUI gui
    private final HashMap<UUID, Integer> guiPlayerList;                   //UUID playerUuid, int guiId
    private int lastId;

    public GuiManager(){
        this.guiList = new HashMap<>();
        this.guiPlayerList = new HashMap<>();
        this.lastId = -1;

        HCore.getInstance().getPacketManager().addInPacketRunnable(new GuiClickRunnable(this));
    }

    public int addGui(String title, List<ItemStack> itemStackList){
        int id = ++this.lastId;
        HuskiGui huskiGui = new HuskiGui(101, title, itemStackList);
        guiList.put(id, huskiGui);

        return id;
    }

    public boolean removeGui(int guiId){
        return guiList.remove(guiId) != null;
    }

    public void openGui(Player player, int guiId){
        HuskiGui huskiGui = guiList.get(guiId);

        if (huskiGui != null){
            huskiGui.openWindow(player);
        }

        openedWindow(player, guiId);
    }

    public void reloadGui(Player player, int guiId){
        System.out.println("Trying to reload");
        HuskiGui huskiGui = guiList.get(guiId);
        System.out.println("guiId: " + guiId);

        if (huskiGui != null){
            huskiGui.reloadWindow(player);
            System.out.println("ran HuskiGui.reloadWindow()");
        }
    }

    public void closeGui(Player player, int guiId){
        HuskiGui huskiGui = guiList.get(guiId);

        if (huskiGui != null){
            huskiGui.closeWindow(player);

        }

        closedWindow(player);
    }

    public boolean isEmpty(){
        return guiList.isEmpty();
    }

    public void openedWindow(Player player, int guiId){
        guiPlayerList.put(player.getUniqueId(), guiId);
    }

    public void closedWindow(Player player){
        guiPlayerList.remove(player.getUniqueId());
    }

    public int currentGui(Player player){
        Integer guiId = guiPlayerList.get(player.getUniqueId());
        if (guiId != null){
            return guiId;
        }
        else {
            return -1;
        }
    }
}
