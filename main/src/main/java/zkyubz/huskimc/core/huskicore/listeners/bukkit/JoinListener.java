package zkyubz.huskimc.core.huskicore.listeners.bukkit;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.listeners.bukkit.tasks.PlayerInjector;
import zkyubz.huskimc.core.huskicore.managers.NmsPacketManager;
import zkyubz.huskimc.core.huskicore.managers.holograms.HologramManager;
import zkyubz.huskimc.core.huskicore.managers.player.PlayerTrackerManager;
import zkyubz.huskimc.core.huskicore.managers.scoreboard.ScoreboardMgr;

public class JoinListener implements Listener {
    private final HCore hCore;
    private final ScoreboardMgr scoreMgr;
    private final PlayerTrackerManager trackerMgr;
    private final HologramManager hologramMgr;

    public JoinListener(HCore hCore){
        this.hCore = hCore;
        this.scoreMgr = hCore.getScoreboardManager();
        this.trackerMgr = hCore.getPlayerTrackerManager();
        this.hologramMgr = hCore.getHologramManager();
    }

    @EventHandler()
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        NmsPacketManager nmsPacketManager = hCore.getPacketManager();

        PlayerInjector playerInjector = new PlayerInjector(player, nmsPacketManager);
        playerInjector.runTaskAsynchronously(hCore);

        scoreMgr.addPlayer(player);
        trackerMgr.addPlayer(player);
        hologramMgr.addPlayer(player);
    }
}
