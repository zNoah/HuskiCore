package zkyubz.huskimc.core.huskicore.managers.holograms.variables;

import org.bukkit.entity.Player;

public interface HoloButtonAction {
    void run(HuskiHologram huskiHologram, Player player);
}
