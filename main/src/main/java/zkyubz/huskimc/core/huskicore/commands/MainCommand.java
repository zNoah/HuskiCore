package zkyubz.huskimc.core.huskicore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.utils.ChatColor;

public class MainCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        defaultMessage(sender);
        return true;
    }

    public void defaultMessage(CommandSender sender){
        sender.sendMessage(ChatColor.format("&8."));
        sender.sendMessage(ChatColor.format("       &b&lHuskiCore " + HCore.getInstance().getDescription().getVersion() + " "));
        sender.sendMessage(ChatColor.format("  "));
        sender.sendMessage(ChatColor.format(" ➜ &fCreado por &bNua#1962"));
        sender.sendMessage(ChatColor.format(" ➜ &fPara: &aHuskiMC"));
        sender.sendMessage(ChatColor.format("&8."));
    }
}
