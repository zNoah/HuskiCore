package zkyubz.huskimc.core.huskicore.events.huskicore;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class HCoreLoadedEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    public HCoreLoadedEvent(){
        //Plugin loaded completely
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}