package zkyubz.huskimc.core.huskicore.managers.player.variables;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import zkyubz.huskimc.core.huskicore.HCore;
import zkyubz.huskimc.core.huskicore.managers.player.PlayerTrackerManager;
import zkyubz.huskimc.core.huskicore.managers.player.variables.PlayerTrackerRunnable;

import java.util.List;

public class PlayerTracker extends BukkitRunnable {
    private final PlayerTrackerManager playerTrackerManager;
    private final Player player;
    private double lastX;
    private double lastY;
    private double lastZ;

    public PlayerTracker(PlayerTrackerManager playerTrackerManager, Player player){
        this.playerTrackerManager = playerTrackerManager;
        this.player = player;

        this.lastX = player.getLocation().getX();
        this.lastY = player.getLocation().getY();
        this.lastZ = player.getLocation().getZ();
    }

    @Override
    public void run() {
        double currentX = player.getLocation().getX();
        double currentY = player.getLocation().getY();
        double currentZ = player.getLocation().getZ();

        if (currentX == lastX && currentY == lastY && currentZ == lastZ){
            return;
        }
        else {
            List<PlayerTrackerRunnable> runnableList = playerTrackerManager.getRunnableList();
            for (int i = 0; i < runnableList.size(); i++){
                runnableList.get(i).run(player, currentX, currentY, currentZ);
            }
            this.lastX = player.getLocation().getX();
            this.lastY = player.getLocation().getY();
            this.lastZ = player.getLocation().getZ();
        }
    }
}
