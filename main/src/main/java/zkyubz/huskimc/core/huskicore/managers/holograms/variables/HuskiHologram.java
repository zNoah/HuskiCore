package zkyubz.huskimc.core.huskicore.managers.holograms.variables;

import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.HologramFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;
import zkyubz.huskimc.core.huskicore.utils.placeholderapi.PlaceholderUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class HuskiHologram {
    private UUID holoUUID;
    private UUID worldUUID;
    private double x;
    private double y;
    private double z;
    private boolean spawnByDefault;
    private boolean useMarker;
    private double yOffsetPerLine;
    private boolean interactive;

    private List<WrappedPacket> wrappedSpawnEntities;
    private List<WrappedPacket> wrappedEntitiesDestroy;
    private List<Object> nmsEntities;
    private List<String> lineList;

    private HoloButton holoButton;

    public HuskiHologram(UUID worldUUID, double x, double y, double z, List<String> lineList){
        this.worldUUID = worldUUID;
        this.x = x;
        this.y = y;
        this.z = z;
        this.spawnByDefault = true;
        this.useMarker = true;
        this.yOffsetPerLine = -0.25;
        this.interactive = false;

        replaceAll(lineList);
    }

    //public HuskiHologram(UUID worldUUID, double x, double y, double z, List<String> lineList, double yOffsetPerLine);

    public HuskiHologram(UUID worldUUID, double x, double y, double z, List<String> lineList, boolean spawnByDefault){
        this.worldUUID = worldUUID;
        this.x = x;
        this.y = y;
        this.z = z;
        this.spawnByDefault = spawnByDefault;
        this.useMarker = true;
        this.yOffsetPerLine = -0.25;
        this.interactive = false;

        replaceAll(lineList);
    }

    public HuskiHologram(UUID worldUUID, double x, double y, double z, List<String> lineList, boolean spawnByDefault, boolean interactive){
        this.worldUUID = worldUUID;
        this.x = x;
        this.y = y;
        this.z = z;
        this.spawnByDefault = spawnByDefault;
        this.useMarker = true;
        this.yOffsetPerLine = -0.25;
        this.interactive = interactive;

        replaceAll(lineList);
    }

    public HuskiHologram(UUID worldUUID, double x, double y, double z, double yOffsetPerLine, boolean spawnByDefault, boolean useMarker){
        this.worldUUID = worldUUID;
        this.x = x;
        this.y = y;
        this.z = z;
        this.spawnByDefault = spawnByDefault;
        this.useMarker = useMarker;
        this.yOffsetPerLine = yOffsetPerLine;
        this.interactive = false;
    }

    //public HuskiHologram(UUID worldUUID, double x, double y, double z, List<String> lineList, double yOffsetPerLine, boolean spawnByDefault);

    private void generateButton(){
        if (holoButton != null){
            holoButton.unregisterPacketListener();
        }
        this.holoButton = new HoloButton(this);
    }

    public synchronized void spawnHologram(Player player){
        HologramFactory factory = new HologramFactory();
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);
        assert nmsPlayer != null;

        for (int i = 0; i < lineList.size(); i++){
            String line = lineList.get(i);
            factory.setNmsEntity(nmsEntities.get(i));
            WrappedPacket wrappedEntityData;
            if (line.equals("")){
                wrappedEntityData = factory.buildHologramData(useMarker);
            }
            else {
                wrappedEntityData = factory.buildHologramData(PlaceholderUtil.papiParse(player, line), useMarker);
            }

            nmsPlayer.sendPacket(wrappedSpawnEntities.get(i).getPacket());
            nmsPlayer.sendPacket(wrappedEntityData.getPacket());
        }

        if (interactive){
            holoButton.spawnHologram(player);
        }
    }

    public synchronized void refreshHologram(Player player){
        HologramFactory factory = new HologramFactory();
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);
        assert nmsPlayer != null;

        for (int i = 0; i < lineList.size(); i++){
            String line = lineList.get(i);
            factory.setNmsEntity(nmsEntities.get(i));
            WrappedPacket wrappedEntityData = factory.buildHologramData(PlaceholderUtil.papiParse(player, line));

            nmsPlayer.sendPacket(wrappedEntityData.getPacket());
        }
    }

    public synchronized void playerUpdateLine(Player player, int index){
        HologramFactory factory = new HologramFactory();
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);
        assert nmsPlayer != null;

        String line = lineList.get(index);
        factory.setNmsEntity(nmsEntities.get(index));
        WrappedPacket wrappedEntityData;
        if (line.equals("")){
            wrappedEntityData = factory.buildHologramData();
        }
        else {
            wrappedEntityData = factory.buildHologramData(PlaceholderUtil.papiParse(player, line));
        }


        nmsPlayer.sendPacket(wrappedEntityData.getPacket());
    }

    public void despawnHologram(Player player){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);
        assert nmsPlayer != null;

        for (int i = 0; i < lineList.size(); i++){
            nmsPlayer.sendPacket(wrappedEntitiesDestroy.get(i).getPacket());
        }

        if (interactive){
            holoButton.despawnHologram(player);
        }
    }

    public void setLine(int index, String lineText) throws ArrayIndexOutOfBoundsException {
        lineList.set(index, lineText);
    }

    //Todo: Use entityNMS if possible
    public synchronized void replaceAll(List<String> lineList){
        HologramFactory factory = new HologramFactory();

        List<WrappedPacket> wrappedSpawnEntities = new ArrayList<>();
        List<WrappedPacket> wrappedEntitiesDestroy = new ArrayList<>();
        List<Object> nmsEntities = new ArrayList<>();

        double yOffset = 0;
        for (int i = 0; i < lineList.size(); i++){
            wrappedSpawnEntities.add(factory.buildHologramEntity(x, y + yOffset, z));
            wrappedEntitiesDestroy.add(factory.buildDestroyHologram());
            nmsEntities.add(factory.getNmsEntity());

            yOffset += yOffsetPerLine;
        }

        this.wrappedSpawnEntities = wrappedSpawnEntities;
        this.wrappedEntitiesDestroy = wrappedEntitiesDestroy;
        this.nmsEntities = nmsEntities;
        this.lineList = lineList;

        if (interactive){
            generateButton();
        }
    }

    public void playerUpdateLoc(Player player){
        HologramFactory factory = new HologramFactory();
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);

        double yOffset = 0;
        for (int i = 0; i < lineList.size(); i++){
            factory.setNmsEntity(nmsEntities.get(i));
            WrappedPacket wrappedEntityTeleport = factory.buildTeleportHologram(x, y + yOffset, z);

            assert nmsPlayer != null;
            nmsPlayer.sendPacket(wrappedEntityTeleport.getPacket());
            yOffset += yOffsetPerLine;
        }
    }

    public void setHoloUUID(UUID holoUUID){
        this.holoUUID = holoUUID;
    }

    public void setNewLocation(UUID worldUUID, double x, double y, double z){
        this.worldUUID = worldUUID;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setNewLocation(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public UUID getHoloUUID(){
        return holoUUID;
    }

    public UUID getWorldUUID(){
        return worldUUID;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public boolean spawnByDefault(){
        return spawnByDefault;
    }

    public int getLineAmount(){
        return lineList.size();
    }

    public List<WrappedPacket> getWrappedSpawnEntities(){
        return wrappedSpawnEntities;
    }

    public List<Object> getNmsEntities(){
        return nmsEntities;
    }

    public HoloButton getHoloButton(){
        return holoButton;
    }
}
