package zkyubz.huskimc.core.huskicore.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySQLHandler {
    private final Connection selectPool;
    private final Connection insertPool;
    private final Connection updatePool;

    public MySQLHandler(String address, String username, String password, String databaseName) throws SQLException {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://" + address + "/" + databaseName + "");
        config.setUsername(username);
        config.setPassword(password);
        config.addDataSourceProperty("useServerPrepStmts", "true");
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        HikariDataSource dataSource = new HikariDataSource(config);

        selectPool = dataSource.getConnection();
        insertPool = dataSource.getConnection();
        updatePool = dataSource.getConnection();
    }

    public List<Object> getData(String tableName, String searchColumn, String key, String column) throws SQLException {
        List<Object> dataList = new ArrayList<>();
        String query = "select " + column + " from " + tableName + " where " + searchColumn + "=" + key;

        PreparedStatement pst = selectPool.prepareStatement(query);
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            dataList.add(rs.getObject(column));
        }
        return dataList;
    }

    public HashMap<String, Object> getData(String tableName, String searchColumn, String key) throws SQLException {
        HashMap<String, Object> data = new HashMap<>();
        String query = "select * from " + tableName + " where " + searchColumn + "=" + key;
        System.out.println(query); //Debug

        PreparedStatement pst = selectPool.prepareStatement(query);
        ResultSet rs = pst.executeQuery();

        ResultSetMetaData metadata = rs.getMetaData();
        int columnCount = metadata.getColumnCount();

        while (rs.next()) {
            for (int i = 1; i <= columnCount; i++) {
                data.put(metadata.getColumnName(i), rs.getObject(i));
            }
        }

        return data;
    }

    public List<List <Object>> getData(String tableName, String searchColumn, String key, List<String> columns) throws SQLException {
        List<List <Object>> dataMatrix = new ArrayList<>();
        String columnsString = listToStringColumns(columns);
        String query = "select " + columnsString + " from " + tableName + " where " + searchColumn + "=" + key;
        System.out.println(query); //Debug

        PreparedStatement pst = selectPool.prepareStatement(query);
        ResultSet rs = pst.executeQuery();

        while (rs.next()) {
            List<Object> dataList = new ArrayList<>();

            for (String column : columns) {
                dataList.add(rs.getObject(column));
            }

            dataMatrix.add(dataList);
        }
        return dataMatrix;
    }

    public int insertData(String tableName, List<Object> dataList, List<String> columns) throws SQLException {
        String dataListString = listToStringValues(dataList);
        String columnsString = listToStringColumns(columns);
        String query = "insert into " + tableName + " (" + columnsString + ") values (" + dataListString + ")";
        System.out.println(query); //Debug

        PreparedStatement pst = insertPool.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        pst.executeUpdate();

        ResultSet rs = pst.getGeneratedKeys();
        rs.next();

        return rs.getInt(1);
    }

    public boolean updateData(String tableName, int id, List<Object> dataList, List<String> columns) throws SQLException {
        String query = "update " + tableName + " set " + updateString(dataList, columns) + " where id=" + id;
        System.out.println(query); //Debug

        PreparedStatement pst = updatePool.prepareStatement(query);
        pst.execute();

        return true;
    }

    public boolean executeQuery(String query) throws SQLException {
        PreparedStatement pst = insertPool.prepareStatement(query);
        pst.executeQuery();

        return true;
    }

    private String listToStringColumns(List<?> list){
        String stringToReturn = "";
        boolean first = true;

        for (Object object : list){
            if (first){
                first = false;
            }
            else {
                stringToReturn += ",";
            }
            stringToReturn += object.toString();
        }

        return stringToReturn;
    }

    private String listToStringValues(List<?> list){
        String stringToReturn = "";
        boolean first = true;

        for (Object object : list){
            if (first){
                first = false;
            }
            else {
                stringToReturn += ",";
            }

            if (object instanceof String){
                stringToReturn += "\"" + object + "\"";
            }
            else {
                stringToReturn += object.toString();
            }
        }
        return stringToReturn;
    }

    private String listToStringValuesAlt(List<?> list){
        String stringToReturn = "";
        boolean first = true;

        for (Object object : list){
            if (first){
                first = false;
            }
            else {
                stringToReturn += ",";
            }

            if (object instanceof String){
                stringToReturn += "\\\"" + object + "\\\"";
            }
            else {
                stringToReturn += object.toString();
            }
        }
        return stringToReturn;
    }

    private String updateString(List<?> dataList, List<String> columns){
        String stringToReturn = "";
        boolean first = true;

        for (int i = 0; i < dataList.size(); ++i){
            if (first){
                first = false;
            }
            else {
                stringToReturn += ",";
            }

            if (dataList.get(i) instanceof String){
                stringToReturn += columns.get(i) + "=\"" + dataList.get(i).toString() + "\"";
            }
            else {
                stringToReturn += columns.get(i) + "=" + dataList.get(i).toString();
            }
        }

        return stringToReturn;
    }
}
