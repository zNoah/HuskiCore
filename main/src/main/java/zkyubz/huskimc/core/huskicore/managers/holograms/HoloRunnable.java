package zkyubz.huskimc.core.huskicore.managers.holograms;

import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.managers.player.variables.PlayerTrackerRunnable;

public class HoloRunnable implements PlayerTrackerRunnable {
    private final HologramManager hologramManager;

    public HoloRunnable(HologramManager hologramManager){
        this.hologramManager = hologramManager;
    }

    @Override
    public void run(Player player, double x, double y, double z) {
        hologramManager.spawnIfNear(player);
    }
}
