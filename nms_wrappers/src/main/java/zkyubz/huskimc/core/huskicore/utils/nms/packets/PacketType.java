package zkyubz.huskimc.core.huskicore.utils.nms.packets;

/***
 * Add new packet types if more is needed
 */

public enum PacketType {
    //In Entity packets
    USE_ENTITY,

    //In Window packets
    WINDOW_CLICK,

    //Out Entity packets
    SPAWN_ENTITY,
    ENTITY_METADATA,
    ENTITY_DESTROY,
    ENTITY_TELEPORT,

    //Out Score packets
    SCOREBOARD_OBJECTIVE,
    SCOREBOARD_DISPLAY,
    SCOREBOARD_SCORE,
    SCOREBOARD_TEAM,

    //Out Window packets
    OPEN_WINDOW,
    CLOSE_WINDOW,
    WINDOW_ITEMS,
    SET_SLOT,

    //Out Player packets
    NAMED_ENTITY_SPAWN,
    PLAYER_INFO,

    //Out World packets
    UPDATE_TIME,

    //Out Utils packets
    TITLE,
    CHAT,

    //Unknown
    UNKNOWN
}
