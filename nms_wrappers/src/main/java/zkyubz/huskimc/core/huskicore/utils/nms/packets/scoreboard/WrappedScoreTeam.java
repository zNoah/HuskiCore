package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedScoreTeam extends WrappedPacket {
    void update(String text);
}
