package zkyubz.huskimc.core.huskicore.utils.nms.packets.window;

import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedWindowClick extends WrappedPacket {
    int getWindowId();
    int getSlot();
    int getButtonPressed();
    ItemStack getBukkitItemStack();
    Object getNmsItemStack();
    int getMode();
}
