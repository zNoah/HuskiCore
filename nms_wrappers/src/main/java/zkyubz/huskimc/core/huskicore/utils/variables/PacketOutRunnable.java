package zkyubz.huskimc.core.huskicore.utils.variables;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface PacketOutRunnable {
    void execute(Player player, ChannelHandlerContext channelHandlerContext, WrappedPacket wrappedPacket, ChannelPromise channelPromise);
    boolean cancelPacket();
}
