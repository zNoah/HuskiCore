package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedSpawnEntity extends WrappedPacket {
    int getEntityID();
    Object getNmsEntity();
    void update(String name, double x, double y, double z);
}
