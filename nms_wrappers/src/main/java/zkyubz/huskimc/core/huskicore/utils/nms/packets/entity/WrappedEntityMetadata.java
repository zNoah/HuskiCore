package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedEntityMetadata extends WrappedPacket {
    void update(String name);
}
