package zkyubz.huskimc.core.huskicore.utils.nms.packets.window;

import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedSetSlot extends WrappedPacket {
    int getWindowId();
    int getSlot();
    ItemStack getBukkitItemStack();
    Object getNmsItemStack();
}
