package zkyubz.huskimc.core.huskicore.utils.nms.packets.player;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

import java.util.UUID;

public interface WrappedNamedEntitySpawn extends WrappedPacket {
    UUID playerUUID();
}
