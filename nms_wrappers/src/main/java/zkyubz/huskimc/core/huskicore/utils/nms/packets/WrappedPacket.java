package zkyubz.huskimc.core.huskicore.utils.nms.packets;

public interface WrappedPacket {
    Object getPacket();
    PacketType getPacketType();
}
