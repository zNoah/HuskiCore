package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedUseEntity extends WrappedPacket {
    int getEntityId();
    Object getNmsEntity(Object nmsWorld);
    int getAction();
}
