package zkyubz.huskimc.core.huskicore.utils.nms.packets.player;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

import java.util.List;

public interface WrappedPlayerInfo extends WrappedPacket {
    int getMode();
    List<WrappedPlayerInfoData> playerInfoDataList();
}
