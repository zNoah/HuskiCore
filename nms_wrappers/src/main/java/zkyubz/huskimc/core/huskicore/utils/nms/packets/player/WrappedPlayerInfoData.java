package zkyubz.huskimc.core.huskicore.utils.nms.packets.player;

import java.util.UUID;

public interface WrappedPlayerInfoData {
    //GameProfile getProfile();
    UUID playerUUID();
}
