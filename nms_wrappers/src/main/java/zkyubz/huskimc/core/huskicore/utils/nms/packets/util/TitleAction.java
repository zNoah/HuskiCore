package zkyubz.huskimc.core.huskicore.utils.nms.packets.util;

public enum TitleAction {
    TITLE, SUBTITLE, TIMES, CLEAR, RESET
}
