package zkyubz.huskimc.core.huskicore.utils.variables;

import io.netty.channel.ChannelHandlerContext;
import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface PacketInRunnable {
    void execute(Player player, ChannelHandlerContext channelHandlerContext, WrappedPacket wrappedPacket);
}
