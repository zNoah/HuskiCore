package zkyubz.huskimc.core.huskicore.utils.nms.packets.util;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public interface WrappedUpdateTime extends WrappedPacket {
    long getTime();
    long getAge();
}
