package zkyubz.huskimc.core.huskicore.utils.nms.player;

public interface NMSPlayer {
    void sendPacket(Object packet);
    Object getPipeline();
}
