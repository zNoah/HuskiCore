package zkyubz.huskimc.core.huskicore.utils;

public class ColorUtil {
    private final static String[] colors = new String[16];
    private final static String[] formats = new String[5];
    private final static String symbol = "§";

    public static String getColor(int index){
        return symbol + colors[index];
    }

    public static String getLastColor(String text){
        String lastColorFound = "§r";

        for (int i = 0; i < text.length(); i++){
            if (text.charAt(i) == '§'){
                if (isColor(text.charAt(i + 1))){
                    lastColorFound = symbol + text.charAt(i + 1);
                }
                else {
                    lastColorFound += symbol + text.charAt(i + 1);
                }
            }
        }

        return lastColorFound;
    }

    @Deprecated
    public static String[] getLines(String prefix, String suffix){
        String lastColorFound = "§r";

        for (int i = 0; i < prefix.length(); i++){
            if (prefix.charAt(i) == '§'){
                if ((i + 1) < prefix.length()){
                    if (isColor(prefix.charAt(i + 1))){
                        lastColorFound = symbol + prefix.charAt(i + 1);
                    }
                    else {
                        lastColorFound += symbol + prefix.charAt(i + 1);
                    }
                }
                else {
                    prefix = prefix.substring(0, prefix.length() - 1);
                    suffix = suffix.substring(1);
                }
            }
        }

        String[] result = new String[3];
        result[0] = lastColorFound;
        result[1] = prefix;
        result[2] = suffix;

        return result;
    }

    public static boolean isColor(char c){
        for (int i = 0; i < colors.length; i++){
            if (c == colors[i].charAt(0)){
                return true;
            }
        }

        return false;
    }

    public static void setValues(){
        colors[0] = "0";
        colors[1] = "1";
        colors[2] = "2";
        colors[3] = "3";
        colors[4] = "4";
        colors[5] = "5";
        colors[6] = "6";
        colors[7] = "7";
        colors[8] = "8";
        colors[9] = "9";
        colors[10] = "a";
        colors[11] = "b";
        colors[12] = "c";
        colors[13] = "d";
        colors[14] = "e";
        colors[15] = "f";

        formats[0] = "k";
        formats[1] = "l";
        formats[2] = "m";
        formats[3] = "n";
        formats[4] = "o";
    }
}
