package zkyubz.huskimc.core.huskicore.utils.placeholderapi;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class PlaceholderUtil {
    private static boolean usePapi = false;

    public static String papiParse(Player player, String text){
        if (usePapi && player != null){
            text = PlaceholderAPI.setPlaceholders(player, text);
        }
        else {
            text = papiClear(text); //Remove placeholders
        }
        return text;
    }

    public static String papiClear(String text){
        int startPlaceholder = -1;
        int endPlaceholder;

        for (int i = 0; i < text.length(); i++){
            if (text.charAt(i) == '%'){
                if (startPlaceholder == -1){
                    startPlaceholder = i;
                }
                else {
                    endPlaceholder = i;
                    text = text.substring(0, startPlaceholder) + text.substring(endPlaceholder + 1);
                    i -= endPlaceholder - startPlaceholder;

                    startPlaceholder = -1;
                }
            }
        }

        return text;
    }

    public static void setUsePapi(boolean enabled){
        usePapi = enabled;
    }
}
