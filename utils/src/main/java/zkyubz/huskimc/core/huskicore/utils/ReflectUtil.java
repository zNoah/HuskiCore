package zkyubz.huskimc.core.huskicore.utils;

import java.lang.reflect.Field;

public class ReflectUtil {
    public static Field createField(Object object, String variableName) throws NoSuchFieldException {
        Field field = object.getClass().getDeclaredField(variableName);
        field.setAccessible(true);
        return field;
    }
}
