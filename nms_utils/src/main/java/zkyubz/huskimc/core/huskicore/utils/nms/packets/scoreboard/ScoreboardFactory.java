package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3.WrappedScoreDisplay_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3.WrappedScore_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3.WrappedScoreTeam_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3.WrappedScoreObjective_v1_8_R3;

public class ScoreboardFactory {
    private Object scoreObjective;

    public ScoreboardFactory(){}

    public ScoreboardFactory(Object scoreObjective){
        this.scoreObjective = scoreObjective;
    }

    public WrappedPacket buildTitlePacket(String name, int scoreId){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                WrappedScoreObjective_v1_8_R3 wrappedTitlePacket = new WrappedScoreObjective_v1_8_R3(name, scoreId);
                this.scoreObjective = wrappedTitlePacket.getScoreObjective();

                return wrappedTitlePacket;
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildDisplayPacket(){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScoreDisplay_v1_8_R3(scoreObjective);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildLinePacket(int index){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScore_v1_8_R3(this.scoreObjective, index, true);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildTeamPacket(String text, int index){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScoreTeam_v1_8_R3(text, index);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    //Remove methods
    public WrappedPacket buildRemoveLinePacket(int index){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScore_v1_8_R3(this.scoreObjective, index, false);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildRemoveTeamPacket(int index){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScoreTeam_v1_8_R3(index, true);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildRemoveScorePacket(int scoreId){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScoreObjective_v1_8_R3(scoreId);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildLinkTeamPacket(int index){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedScoreTeam_v1_8_R3(index, false);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public Object getScoreObjective(){
        return this.scoreObjective;
    }
}
