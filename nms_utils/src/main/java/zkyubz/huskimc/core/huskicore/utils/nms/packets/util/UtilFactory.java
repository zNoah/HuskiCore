package zkyubz.huskimc.core.huskicore.utils.nms.packets.util;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.v1_8_R3.WrappedChat_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.v1_8_R3.WrappedTitle_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.v1_8_R3.WrappedUpdateTime_v1_8_R3;

public class UtilFactory {
    //Builders
    public WrappedPacket buildTitlePacket(String text, TitleAction titleAction){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedTitle_v1_8_R3(text, titleAction);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildTitlePacket(int fadeIn, int stay, int fadeOut){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedTitle_v1_8_R3(fadeIn, stay, fadeOut);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildChatPacket(String text, ChatType chatType){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedChat_v1_8_R3(text, chatType);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildTimePacket(long age, long time){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedUpdateTime_v1_8_R3(age, time);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildTimePacket(long time){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedUpdateTime_v1_8_R3(time, time);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    //Wrappers
    public WrappedPacket wrapTimePacket(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedUpdateTime_v1_8_R3(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }
}
