package zkyubz.huskimc.core.huskicore.utils.nms.exception;

public class UnsupportedServerVersionException extends Exception {
    public UnsupportedServerVersionException(String version){
        super("Server version " + version + " is not supported ):");
    }
}
