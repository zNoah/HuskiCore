package zkyubz.huskimc.core.huskicore.utils.nms.packets.window;

import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.v1_8_R3.*;

import java.util.List;

public class WindowFactory {
    private int windowId;

    public WindowFactory(){}

    //Builder methods
    public WrappedPacket buildOpenWindowPacket(int windowId, String title, int size){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();
        this.windowId = windowId;

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedOpenWindow_v1_8_R3(windowId, title, size);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildCloseWindowPacket(){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedCloseWindow_v1_8_R3(this.windowId);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedWindowItems buildWindowItemsPacket(List<ItemStack> itemStackList, Object player){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedWindowItems_v1_8_R3(this.windowId, player, itemStackList);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildSetSlotPacket(int windowId, int slot, ItemStack itemStack){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedSetSlot_v1_8_R3(windowId, slot, itemStack);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPacket buildSetSlotPacket(int windowId, int slot, Object nmsItemStack){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedSetSlot_v1_8_R3(windowId, slot, nmsItemStack);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    //Wrapper methods
    public WrappedWindowClick wrapWindowClickPacket(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedWindowClick_v1_8_R3(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }
}
