package zkyubz.huskimc.core.huskicore.utils.nms;

import org.bukkit.World;
import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.world.v1_8_R3.WorldUtil_v1_8_R3;

public class NMSUtil {
    private static NMSVersion nmsVersion;

    public static NMSPlayer getNMSPlayer(Player player){
        switch (nmsVersion){
            case v1_8_R3:
                return new NMSPlayer_v1_8_R3(player);
            case v1_12_R1:
            case v1_17_R1:
            default:
                return null;
        }
    }

    public static Object getNmsWorld(World world){
        switch (nmsVersion){
            case v1_8_R3:
                return WorldUtil_v1_8_R3.getNmsWorld(world);
            case v1_12_R1:
            case v1_17_R1:
            default:
                return null;
        }
    }

    public static NMSVersion getNMSVersion(){
        return nmsVersion;
    }

    public static void setNMSVersion(NMSVersion nmsVersion){
        NMSUtil.nmsVersion = nmsVersion;
    }
}
