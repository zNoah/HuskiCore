package zkyubz.huskimc.core.huskicore.utils.nms.nbt;

import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.nbt.v1_8_R3.NbtModifier_v1_8_R3;

public class NbtModifier {
    public static ItemStack setTag(ItemStack item, String key, int value){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return NbtModifier_v1_8_R3.setTag(item, key, value);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public static ItemStack setTag(ItemStack item, String key, String value){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return NbtModifier_v1_8_R3.setTag(item, key, value);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }
}
