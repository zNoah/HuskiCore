package zkyubz.huskimc.core.huskicore.utils.nms.packets.player;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.v1_8_R3.WrappedNamedEntitySpawn_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.v1_8_R3.WrappedPlayerInfo_v1_8_R3;

public class PlayerPacketFactory {
    //Wrapper methods
    public WrappedNamedEntitySpawn wrapNamedEntitySpawnPacket(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedNamedEntitySpawn_v1_8_R3(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedPlayerInfo wrapPlayerInfoSpawnPacket(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedPlayerInfo_v1_8_R3(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }
}
