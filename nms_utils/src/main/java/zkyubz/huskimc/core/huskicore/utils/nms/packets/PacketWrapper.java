package zkyubz.huskimc.core.huskicore.utils.nms.packets;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.EntityFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.PlayerPacketFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.UtilFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WindowFactory;

public class PacketWrapper {
    public static PacketType getTypeIn(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return PacketIdentifier_v1_8_R3.getTypeIn(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public static PacketType getTypeOut(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return PacketIdentifier_v1_8_R3.getTypeOut(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public static WrappedPacket wrapInPacket(Object packet){
        PacketType type = getTypeIn(packet);

        switch (type){
            case USE_ENTITY:
                EntityFactory entityFactory = new EntityFactory();
                return entityFactory.wrapUseEntityPacket(packet);
            case WINDOW_CLICK:
                WindowFactory windowFactory = new WindowFactory();
                return windowFactory.wrapWindowClickPacket(packet);
        }

        return null;
    }

    public static WrappedPacket wrapOutPacket(Object packet){
        PacketType type = getTypeOut(packet);

        switch (type){
            case NAMED_ENTITY_SPAWN:
                PlayerPacketFactory playerPacketFactory = new PlayerPacketFactory();
                return playerPacketFactory.wrapNamedEntitySpawnPacket(packet);
            case PLAYER_INFO:
                playerPacketFactory = new PlayerPacketFactory();
                return playerPacketFactory.wrapPlayerInfoSpawnPacket(packet);
            case UPDATE_TIME:
                UtilFactory utilFactory = new UtilFactory();
                return utilFactory.wrapTimePacket(packet);
        }

        return null;
    }
}
