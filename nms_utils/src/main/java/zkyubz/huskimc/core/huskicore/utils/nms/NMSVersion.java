package zkyubz.huskimc.core.huskicore.utils.nms;

public enum NMSVersion {
    v1_8_R3,
    v1_12_R1,
    v1_17_R1,
    UNSUPPORTED
}
