package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3.WrappedEntityDestroy_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3.WrappedEntityMetadata_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3.WrappedEntityTeleport_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3.WrappedSpawnEntity_v1_8_R3;

public class HologramFactory {
    private Object nmsEntity;
    private int entityID;

    //public HologramFactory()

    public WrappedSpawnEntity buildHologramEntity(double x, double y, double z){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                WrappedSpawnEntity_v1_8_R3 wrappedSpawnEntity = new WrappedSpawnEntity_v1_8_R3(x, y, z);
                this.nmsEntity = wrappedSpawnEntity.getNmsEntity();
                this.entityID = wrappedSpawnEntity.getEntityID();

                return wrappedSpawnEntity;
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedEntityMetadata buildHologramData(String name){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityMetadata_v1_8_R3(name, nmsEntity);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedEntityMetadata buildHologramData(boolean useMarker){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityMetadata_v1_8_R3(nmsEntity, useMarker);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedEntityMetadata buildHologramData(String name, boolean useMarker){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityMetadata_v1_8_R3(name, nmsEntity, useMarker);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedEntityMetadata buildHologramData(){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityMetadata_v1_8_R3(nmsEntity);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedEntityDestroy buildDestroyHologram(){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityDestroy_v1_8_R3(entityID);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public WrappedEntityTeleport buildTeleportHologram(double x, double y, double z){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityTeleport_v1_8_R3(nmsEntity, x, y, z);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }

    public void setNmsEntity(Object nmsEntity){
        this.nmsEntity = nmsEntity;
    }

    public Object getNmsEntity(){
        return nmsEntity;
    }

    /*public WrappedEntityMetadata buildHologramData(){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedEntityMetadata_v1_8_R3(nmsEntity);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }*/
}
