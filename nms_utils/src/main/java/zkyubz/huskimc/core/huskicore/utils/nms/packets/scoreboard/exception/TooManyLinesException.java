package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.exception;

public class TooManyLinesException extends Exception {
    public TooManyLinesException(int amount) {
        super("You cannot have more than 16 lines on a scoreboard. Amount: " + amount);
    }
}