package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity;

import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSVersion;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3.WrappedUseEntity_v1_8_R3;

public class EntityFactory {
    public WrappedPacket wrapUseEntityPacket(Object packet){
        NMSVersion nmsVersion = NMSUtil.getNMSVersion();

        switch (nmsVersion){
            case v1_8_R3:
                return new WrappedUseEntity_v1_8_R3(packet);
            case v1_12_R1:
            case v1_17_R1:
        }

        return null;
    }
}
