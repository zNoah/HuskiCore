package zkyubz.huskimc.core.huskicore.api;

import org.bukkit.entity.Player;
import zkyubz.huskimc.core.huskicore.utils.nms.NMSUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.ChatType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.TitleAction;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.UtilFactory;
import zkyubz.huskimc.core.huskicore.utils.nms.player.NMSPlayer;

public class PacketUtils {
    //Create and send packets
    public void createAndSendTimePacket(Player player, long time){
        WrappedPacket wrappedTimePacket = createTimePacket(time);
        sendPacket(player, wrappedTimePacket);
    }

    public void createAndSendActionBarPacket(Player player, String text){
        WrappedPacket wrappedActionBarPacket = createActionBarPacket(text);
        sendPacket(player, wrappedActionBarPacket);
    }

    public void createAndSendTitlePacket(Player player, String title, String subtitle, int fadeIn, int stay, int fadeOut){
        WrappedPacket wrappedTitlePacket = createTitlePacket(title);
        WrappedPacket wrappedSubtitlePacket = createSubtitlePacket(subtitle);
        WrappedPacket wrappedTitleTimePacket = createTitleTimePacket(fadeIn, stay, fadeOut);

        sendPacket(player, wrappedTitleTimePacket);
        sendPacket(player, wrappedSubtitlePacket);
        sendPacket(player, wrappedTitlePacket);
    }

    //Create packets
    public WrappedPacket createTimePacket(long time){
        UtilFactory utilFactory = new UtilFactory();
        return utilFactory.buildTimePacket(time);
    }

    public WrappedPacket createActionBarPacket(String text){
        UtilFactory utilFactory = new UtilFactory();
        return utilFactory.buildChatPacket(text, ChatType.ACTION_BAR);
    }

    public WrappedPacket createTitlePacket(String text){
        UtilFactory utilFactory = new UtilFactory();
        return utilFactory.buildTitlePacket(text, TitleAction.TITLE);
    }

    public WrappedPacket createSubtitlePacket(String text){
        UtilFactory utilFactory = new UtilFactory();
        return utilFactory.buildTitlePacket(text, TitleAction.SUBTITLE);
    }

    public WrappedPacket createTitleTimePacket(int fadeIn, int stay, int fadeOut){
        UtilFactory utilFactory = new UtilFactory();
        return utilFactory.buildTitlePacket(fadeIn, stay, fadeOut);
    }

    //Packet senders
    public void sendPacket(Player player, WrappedPacket wrappedPacket){
        sendPacket(player, wrappedPacket.getPacket());
    }

    public void sendPacket(Player player, Object packet){
        NMSPlayer nmsPlayer = NMSUtil.getNMSPlayer(player);
        assert nmsPlayer != null;
        nmsPlayer.sendPacket(packet);
    }

    //Misc.
    public NMSPlayer getNMSPlayer(Player player){
        return NMSUtil.getNMSPlayer(player);
    }
}
