package zkyubz.huskimc.core.huskicore.api;

import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.nbt.NbtModifier;

public class NbtUtil {
    public ItemStack setTag(ItemStack item, String key, int value){
        return NbtModifier.setTag(item, key, value);
    }

    public ItemStack setTag(ItemStack item, String key, String value){
        return NbtModifier.setTag(item, key, value);
    }
}
