# HuskiCore
A set of common libraries and utilities for Huski Plugins, basically the core for most Huski Plugins

### **Done:**
- MySQL handling
- Scoreboard
- NMS Packet Management
- NBT handling
- GUI/Menu
- Holograms*
- PlayerUtils API

### **Progress:**
- Empty

### **TODO:**
**High priority:**
- Empty

**Lower priority**
- Add more custom exception for better and easier debugging
- Recode packet wrapping
- Code cleanup and small recode
- Multi version support (Time consuming)
- Mongo handling

**Lowest priority**
- Solve spigot enum differences (1.13+)
- Redis handling
- Bytebuf packet management (To reduce the use of Reflection, unknown time consumption)