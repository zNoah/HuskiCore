package zkyubz.huskimc.core.huskicore.utils.nms.player;

import io.netty.channel.ChannelPipeline;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class NMSPlayer_v1_8_R3 implements NMSPlayer {
    private final PlayerConnection playerCon;

    public NMSPlayer_v1_8_R3(Player player){
        this.playerCon = getPlayerCon(player);
    }

    public void sendPacket(Object packet){
        playerCon.sendPacket((Packet<?>) packet);
    }

    public ChannelPipeline getPipeline(){
        return playerCon.networkManager.channel.pipeline();
    }

    public PlayerConnection getPlayerCon(Player player){
        return ((CraftPlayer) player).getHandle().playerConnection;
    }
}
