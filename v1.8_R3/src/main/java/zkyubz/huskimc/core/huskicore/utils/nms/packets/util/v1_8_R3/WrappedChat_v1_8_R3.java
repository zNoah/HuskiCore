package zkyubz.huskimc.core.huskicore.utils.nms.packets.util.v1_8_R3;

import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.ChatType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.WrappedChat;

public class WrappedChat_v1_8_R3 implements WrappedChat {
    private final Object packet;

    public WrappedChat_v1_8_R3(String text, ChatType chatType){
        ChatComponentText textComponent = new ChatComponentText(text);
        byte type;

        switch (chatType){
            case CHAT:
                type = 1;
                break;
            case ACTION_BAR:
            default:
                type = 2;
                break;
        }

        this.packet = new PacketPlayOutChat(textComponent, type);
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.CHAT;
    }
}
