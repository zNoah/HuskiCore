package zkyubz.huskimc.core.huskicore.utils.nms.packets.player.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.WrappedPlayerInfoData;

import java.util.UUID;

public class WrappedPlayerInfoData_v1_8_R3 implements WrappedPlayerInfoData {
    private final Object infoData;

    public WrappedPlayerInfoData_v1_8_R3(Object infoData){
        this.infoData = infoData;
    }

    @Override
    public UUID playerUUID() {
        return ((PacketPlayOutPlayerInfo.PlayerInfoData) infoData).a().getId();
    }
}
