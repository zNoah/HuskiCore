package zkyubz.huskimc.core.huskicore.utils.nms.world.v1_8_R3;

import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

public class WorldUtil_v1_8_R3 {
    public static Object getNmsWorld(org.bukkit.World world){
        return ((CraftWorld) world).getHandle();
    }
}
