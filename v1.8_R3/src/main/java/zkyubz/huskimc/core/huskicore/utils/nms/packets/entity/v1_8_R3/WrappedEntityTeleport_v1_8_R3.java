package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3;

import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedEntityTeleport;

public class WrappedEntityTeleport_v1_8_R3 implements WrappedEntityTeleport {
    private final Object packet;

    public WrappedEntityTeleport_v1_8_R3(Object entity){
        this.packet = new PacketPlayOutEntityTeleport((Entity) entity);
    }

    public WrappedEntityTeleport_v1_8_R3(Object entity, double x, double y, double z){
        ((Entity) entity).locX = x;
        ((Entity) entity).locY = y;
        ((Entity) entity).locZ = z;
        this.packet = new PacketPlayOutEntityTeleport((Entity) entity);
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.ENTITY_TELEPORT;
    }
}
