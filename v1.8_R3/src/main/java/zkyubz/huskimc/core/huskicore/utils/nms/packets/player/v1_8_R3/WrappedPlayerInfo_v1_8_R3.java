package zkyubz.huskimc.core.huskicore.utils.nms.packets.player.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.WrappedPlayerInfo;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.WrappedPlayerInfoData;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class WrappedPlayerInfo_v1_8_R3 implements WrappedPlayerInfo {
    private final Object packet;

    public WrappedPlayerInfo_v1_8_R3(Object packet){
        this.packet = packet;
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.PLAYER_INFO;
    }

    @Override
    public int getMode() {
        try {
            Field field = ((PacketPlayOutPlayerInfo) packet).getClass().getDeclaredField("a");
            field.setAccessible(true);
            return (int) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public List<WrappedPlayerInfoData> playerInfoDataList() {
        try {
            Field field = ((PacketPlayOutPlayerInfo) packet).getClass().getDeclaredField("b");
            field.setAccessible(true);
            List<PacketPlayOutPlayerInfo.PlayerInfoData> playerInfoDataList = (List<PacketPlayOutPlayerInfo.PlayerInfoData>) field.get(packet);

            List<WrappedPlayerInfoData> wrappedPlayerInfoData = new ArrayList<>();

            for (PacketPlayOutPlayerInfo.PlayerInfoData playerInfoData : playerInfoDataList){
                wrappedPlayerInfoData.add(new WrappedPlayerInfoData_v1_8_R3(playerInfoData));
            }

            return wrappedPlayerInfoData;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}