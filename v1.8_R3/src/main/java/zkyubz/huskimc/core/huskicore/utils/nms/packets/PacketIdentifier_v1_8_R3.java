package zkyubz.huskimc.core.huskicore.utils.nms.packets;

import net.minecraft.server.v1_8_R3.*;

public class PacketIdentifier_v1_8_R3 {
    public static PacketType getTypeIn(Object packet){
        if (packet instanceof PacketPlayInWindowClick){
            return PacketType.WINDOW_CLICK;
        }
        else if (packet instanceof PacketPlayInUseEntity){
            return PacketType.USE_ENTITY;
        }
        return PacketType.UNKNOWN;
    }

    public static PacketType getTypeOut(Object packet){
        if (packet instanceof PacketPlayOutNamedEntitySpawn){
            return PacketType.NAMED_ENTITY_SPAWN;
        }
        else if (packet instanceof PacketPlayOutPlayerInfo){
            return PacketType.PLAYER_INFO;
        }
        else if (packet instanceof PacketPlayOutUpdateTime){
            return PacketType.UPDATE_TIME;
        }
        return PacketType.UNKNOWN;
    }
}
