package zkyubz.huskimc.core.huskicore.utils.nms.packets.window.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayOutCloseWindow;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public class WrappedCloseWindow_v1_8_R3 implements WrappedPacket {
    private Object packet;

    public WrappedCloseWindow_v1_8_R3(int windowId){
        this.packet = new PacketPlayOutCloseWindow(windowId);
    }

    public Object getPacket(){
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.CLOSE_WINDOW;
    }
}
