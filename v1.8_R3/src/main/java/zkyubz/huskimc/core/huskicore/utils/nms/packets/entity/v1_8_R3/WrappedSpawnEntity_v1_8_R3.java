package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3;

import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityArmorStand;
import net.minecraft.server.v1_8_R3.PacketPlayOutSpawnEntity;

import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import zkyubz.huskimc.core.huskicore.utils.nms.nbt.v1_8_R3.NbtModifier_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedSpawnEntity;

public class WrappedSpawnEntity_v1_8_R3 implements WrappedSpawnEntity {
    private final int entityId;
    private final Object entity;
    private final Object packet;

    public WrappedSpawnEntity_v1_8_R3(double x, double y, double z){
        EntityArmorStand entity = new EntityArmorStand(null);
        entity.locX = x;
        entity.locY = y;
        entity.locZ = z;

        this.packet = new PacketPlayOutSpawnEntity(entity, 78);
        this.entity = entity;
        this.entityId = entity.getId();
    }

    public WrappedSpawnEntity_v1_8_R3(Object entityObject, double x, double y, double z){
        Entity entity = (Entity) entityObject;
        entity.locX = x;
        entity.locY = y;
        entity.locZ = z;

        this.packet = new PacketPlayOutSpawnEntity(entity, 78);
        this.entity = entity;
        this.entityId = entity.getId();
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.SPAWN_ENTITY;
    }

    @Override
    public int getEntityID() {
        return entityId;
    }

    @Override
    public Object getNmsEntity() {
        return entity;
    }

    //Todo: After testing how spawning entities work
    @Override
    public void update(String name, double x, double y, double z) {
        //PacketPlayOutSpawnEntity packet = (PacketPlayOutSpawnEntity) this.packet;
        //packet.a(MathHelper.floor(x * 32.0D));
        //packet.b()
        //entity.locZ = z;
        //entity.setCustomName(name);
    }
}
