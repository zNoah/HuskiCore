package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3;

import net.minecraft.server.v1_8_R3.*;
import zkyubz.huskimc.core.huskicore.utils.ColorUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.WrappedScoreTeam;
import zkyubz.huskimc.core.huskicore.utils.score.ScoreUtil_v1_8_R3;

import java.util.Collections;

public class WrappedScoreTeam_v1_8_R3 implements WrappedScoreTeam {
    private PacketPlayOutScoreboardTeam packet;
    private ScoreboardTeam scoreTeam;
    private final Integer index;

    //Create
    public WrappedScoreTeam_v1_8_R3(String text, int index){
        Scoreboard scoreObject = ScoreUtil_v1_8_R3.getScoreObject();
        String[] lines = getPrefixSuffix(text);
        ScoreboardTeam scoreTeam = new ScoreboardTeam(scoreObject, Integer.toString(index));
        scoreTeam.setDisplayName("");
        scoreTeam.setPrefix(lines[0]);
        scoreTeam.setSuffix(lines[1]);

        this.packet = new PacketPlayOutScoreboardTeam(scoreTeam,0);
        this.scoreTeam = scoreTeam;
        this.index = index;
    }

    //Remove (or link)
    public WrappedScoreTeam_v1_8_R3(int index, boolean remove){
        Scoreboard scoreObject = ScoreUtil_v1_8_R3.getScoreObject();
        ScoreboardTeam scoreTeam = new ScoreboardTeam(scoreObject, Integer.toString(index));
        PacketPlayOutScoreboardTeam packet;

        if (remove){
            //Creating Team packet
            packet = new PacketPlayOutScoreboardTeam(scoreTeam, 1);
        }
        else {
            packet = new PacketPlayOutScoreboardTeam(scoreTeam, Collections.singletonList(ColorUtil.getColor(index)), 3);
        }

        this.packet = packet;
        this.index = index;
    }

    private String[] getPrefixSuffix(String line){
        String prefix;
        String suffix;

        //Work around to get 28-30 max chars, instead of 16
        if (line.length() > 16){
            prefix = line.substring(0, 16);
            suffix = line.substring(16);

            String[] result = processColors(prefix, suffix);
            prefix = result[0];
            suffix = result[1];

            if (suffix.length() > 16){
                suffix = suffix.substring(0, 16);
            }
        }
        else {
            prefix = line;
            suffix = "";
        }

        String[] lines = new String[2];
        lines[0] = prefix;
        lines[1] = suffix;

        return lines;
    }

    //Gets the color right for the suffix part, don't ask how it works (jk)
    private String[] processColors(String prefix, String suffix){
        String lastColorFound = "§r";
        boolean isLastAColor = false;

        for (int i = 0; i < prefix.length(); i++){
            if (prefix.charAt(i) == '§'){
                if ((i + 1) < prefix.length()){
                    if (ColorUtil.isColor(prefix.charAt(i + 1))){
                        lastColorFound = "§" + prefix.charAt(i + 1);
                        isLastAColor = true;
                    }
                    else {
                        lastColorFound += "§" + prefix.charAt(i + 1);
                    }
                }
                else {
                    if (isLastAColor){
                        lastColorFound += "§" + suffix.charAt(0);
                    }
                    else {
                        lastColorFound = "§" + suffix.charAt(0);
                    }

                    prefix = prefix.substring(0, prefix.length() - 1);
                    suffix = suffix.substring(1);
                }
            }
        }

        String[] result = new String[2];
        result[0] = prefix;
        result[1] = lastColorFound + suffix;

        return result;
    }

    public void update(String text){
        String[] line = getPrefixSuffix(text);

        scoreTeam.setPrefix(line[0]);
        scoreTeam.setSuffix(line[1]);

        this.packet = new PacketPlayOutScoreboardTeam(this.scoreTeam, 2);
    }

    public Object getPacket(){
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.SCOREBOARD_TEAM;
    }
}
