package zkyubz.huskimc.core.huskicore.utils.nms.packets.window.v1_8_R3;

import net.minecraft.server.v1_8_R3.ItemStack;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutSetSlot;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WrappedSetSlot;

import java.lang.reflect.Field;

public class WrappedSetSlot_v1_8_R3 implements WrappedSetSlot {
    private final Object packet;

    public WrappedSetSlot_v1_8_R3(int windowId, int slot, org.bukkit.inventory.ItemStack bukkitItemStack){
        ItemStack nmsItemStack = CraftItemStack.asNMSCopy(bukkitItemStack);

        this.packet = new PacketPlayOutSetSlot(windowId, slot, nmsItemStack);
    }

    public WrappedSetSlot_v1_8_R3(int windowId, int slot, Object nmsItemStack){
        this.packet = new PacketPlayOutSetSlot(windowId, slot, (ItemStack) nmsItemStack);
    }

    public WrappedSetSlot_v1_8_R3(Object packet){
        this.packet = packet;
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.SET_SLOT;
    }

    @Override
    public int getWindowId() {
        try {
            Field field = ((PacketPlayOutPlayerInfo) packet).getClass().getDeclaredField("a");
            field.setAccessible(true);
            return (int) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return -2;
        }
    }

    @Override
    public int getSlot() {
        try {
            Field field = ((PacketPlayOutPlayerInfo) packet).getClass().getDeclaredField("b");
            field.setAccessible(true);
            return (int) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return -2;
        }
    }

    @Override
    public org.bukkit.inventory.ItemStack getBukkitItemStack() {
        try {
            Field field = ((PacketPlayOutPlayerInfo) packet).getClass().getDeclaredField("c");
            field.setAccessible(true);
            Object object = field.get(packet);

            return CraftItemStack.asBukkitCopy((ItemStack) object);

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getNmsItemStack() {
        try {
            Field field = ((PacketPlayOutPlayerInfo) packet).getClass().getDeclaredField("c");
            field.setAccessible(true);
            return field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}
