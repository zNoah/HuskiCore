package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3;

import net.minecraft.server.v1_8_R3.*;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public class WrappedScoreDisplay_v1_8_R3 implements WrappedPacket {
    private final PacketPlayOutScoreboardDisplayObjective packet;

    public WrappedScoreDisplay_v1_8_R3(Object scoreObjective){
        this.packet = new PacketPlayOutScoreboardDisplayObjective(1, (ScoreboardObjective) scoreObjective);
    }

    public Object getPacket(){
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.SCOREBOARD_DISPLAY;
    }
}
