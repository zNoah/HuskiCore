package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3;

import net.minecraft.server.v1_8_R3.IScoreboardCriteria;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardObjective;
import net.minecraft.server.v1_8_R3.Scoreboard;
import net.minecraft.server.v1_8_R3.ScoreboardObjective;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.WrappedScoreObjective;
import zkyubz.huskimc.core.huskicore.utils.score.ScoreUtil_v1_8_R3;

public class WrappedScoreObjective_v1_8_R3 implements WrappedScoreObjective {
    private PacketPlayOutScoreboardObjective packet;
    private ScoreboardObjective scoreObjective;

    //Create
    public WrappedScoreObjective_v1_8_R3(String title, int id){
        Scoreboard scoreObject = ScoreUtil_v1_8_R3.getScoreObject();
        IScoreboardCriteria scoreCriteria = ScoreUtil_v1_8_R3.getScoreCriteria();

        ScoreboardObjective objective = new ScoreboardObjective(scoreObject, Integer.toString(id), scoreCriteria);
        objective.setDisplayName(title);

        this.packet = new PacketPlayOutScoreboardObjective(objective, 0);
        this.scoreObjective = objective;
    }

    //Remove
    public WrappedScoreObjective_v1_8_R3(int id){
        Scoreboard scoreObject = ScoreUtil_v1_8_R3.getScoreObject();
        IScoreboardCriteria scoreCriteria = ScoreUtil_v1_8_R3.getScoreCriteria();

        ScoreboardObjective objective = new ScoreboardObjective(scoreObject, Integer.toString(id), scoreCriteria);

        this.packet = new PacketPlayOutScoreboardObjective(objective, 1);
    }

    public void update(String name){
        scoreObjective.setDisplayName(name);
        this.packet = new PacketPlayOutScoreboardObjective(this.scoreObjective, 2);
    }

    public Object getPacket(){
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.SCOREBOARD_OBJECTIVE;
    }

    public Object getScoreObjective(){
        return scoreObjective;
    }
}
