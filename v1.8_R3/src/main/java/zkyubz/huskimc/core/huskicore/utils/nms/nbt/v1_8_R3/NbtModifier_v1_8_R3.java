package zkyubz.huskimc.core.huskicore.utils.nms.nbt.v1_8_R3;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class NbtModifier_v1_8_R3 {
    public static ItemStack setTag(ItemStack item, String key, int value){
        net.minecraft.server.v1_8_R3.ItemStack itemNms = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = itemNms.hasTag() ? itemNms.getTag() : new NBTTagCompound();
        tag.setInt(key, value);
        itemNms.setTag(tag);
        return CraftItemStack.asBukkitCopy(itemNms);
    }

    public static ItemStack setTag(ItemStack item, String key, String value){
        net.minecraft.server.v1_8_R3.ItemStack itemNms = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = itemNms.hasTag() ? itemNms.getTag() : new NBTTagCompound();
        tag.setString(key, value);
        itemNms.setTag(tag);
        return CraftItemStack.asBukkitCopy(itemNms);
    }
}
