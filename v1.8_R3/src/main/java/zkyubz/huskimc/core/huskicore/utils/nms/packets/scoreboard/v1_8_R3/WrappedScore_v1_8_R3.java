package zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.v1_8_R3;

import net.minecraft.server.v1_8_R3.*;
import zkyubz.huskimc.core.huskicore.utils.ColorUtil;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.scoreboard.WrappedScore;
import zkyubz.huskimc.core.huskicore.utils.score.ScoreUtil_v1_8_R3;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public class WrappedScore_v1_8_R3 implements WrappedScore {
    private final PacketPlayOutScoreboardScore packet;

    public WrappedScore_v1_8_R3(Object scoreObjective, int index, boolean create){
        PacketPlayOutScoreboardScore packet;

        //Create or Update
        if (create){
            Scoreboard scoreObject = ScoreUtil_v1_8_R3.getScoreObject();
            ScoreboardScore scoreboardScore = new ScoreboardScore(scoreObject, (ScoreboardObjective) scoreObjective, ColorUtil.getColor(index));
            scoreboardScore.setScore(15 - index);

            packet = new PacketPlayOutScoreboardScore(scoreboardScore);
        }

        //Delete
        else {
            packet = new PacketPlayOutScoreboardScore(ColorUtil.getColor(index), (ScoreboardObjective) scoreObjective);
        }

        this.packet = packet;
    }

    public Object getPacket(){
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.SCOREBOARD_SCORE;
    }
}
