package zkyubz.huskimc.core.huskicore.utils.nms.packets.window.v1_8_R3;

import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutOpenWindow;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.WrappedPacket;

public class WrappedOpenWindow_v1_8_R3 implements WrappedPacket {
    private final Object packet;

    public WrappedOpenWindow_v1_8_R3(int windowId, String title, int slots){
        ChatComponentText invType = new ChatComponentText(title);
        this.packet = new PacketPlayOutOpenWindow(windowId, "0", invType, slots);
    }

    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.OPEN_WINDOW;
    }
}
