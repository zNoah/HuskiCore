package zkyubz.huskimc.core.huskicore.utils.nms.packets.window.v1_8_R3;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftInventoryPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WrappedWindowItems;

import java.util.ArrayList;
import java.util.List;

public class WrappedWindowItems_v1_8_R3 implements WrappedWindowItems {
    private final Object packet;

    public WrappedWindowItems_v1_8_R3(int windowId, Object player, List<org.bukkit.inventory.ItemStack> bukkitItemStackList){
        List<ItemStack> nmsItemStackList = new ArrayList<>();

        //Adding menu entries
        for (int i = 0; i < bukkitItemStackList.size(); i++){
            ItemStack nmsItemStack = CraftItemStack.asNMSCopy(bukkitItemStackList.get(i));
            nmsItemStackList.add(nmsItemStack);
        }

        CraftPlayer craftPlayer = (CraftPlayer) player;
        PlayerInventory pInventory = ((CraftInventoryPlayer) craftPlayer.getInventory()).getInventory();
        ItemStack[] itemStacks = pInventory.getContents();

        //Add inventory
        for (int i = 9; i < itemStacks.length; i++){
            nmsItemStackList.add(itemStacks[i]);
        }

        //Add hotbar of inventory
        for (int i = 0; i < 9; i++){
            nmsItemStackList.add(itemStacks[i]);
        }

        this.packet = new PacketPlayOutWindowItems(windowId, nmsItemStackList);
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.WINDOW_ITEMS;
    }
}
