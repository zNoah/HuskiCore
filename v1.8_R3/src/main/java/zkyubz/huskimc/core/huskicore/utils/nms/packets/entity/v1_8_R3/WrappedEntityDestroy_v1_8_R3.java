package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedEntityDestroy;

public class WrappedEntityDestroy_v1_8_R3 implements WrappedEntityDestroy {
    private final Object packet;

    public WrappedEntityDestroy_v1_8_R3(int entityId){
        this.packet = new PacketPlayOutEntityDestroy(entityId);
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.ENTITY_DESTROY;
    }
}
