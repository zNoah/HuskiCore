package zkyubz.huskimc.core.huskicore.utils.score;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.IScoreboardCriteria;
import net.minecraft.server.v1_8_R3.Scoreboard;
import net.minecraft.server.v1_8_R3.ScoreboardCriteriaInteger;

import java.util.List;

public class ScoreUtil_v1_8_R3 {
    private static Scoreboard scoreObject;
    private static IScoreboardCriteria scoreCriteria;

    public static Scoreboard getScoreObject(){
        if (scoreObject == null){
            scoreObject = new Scoreboard();
        }
        return scoreObject;
    }

    public static IScoreboardCriteria getScoreCriteria(){
        if (scoreCriteria == null){
            scoreCriteria = createScoreCriteria();
        }
        return scoreCriteria;
    }

    private static IScoreboardCriteria createScoreCriteria(){
        return new IScoreboardCriteria() {
            @Override
            public String getName() {
                return null;
            }

            @Override
            public int getScoreModifier(List<EntityHuman> list) {
                return 0;
            }

            @Override
            public boolean isReadOnly() {
                return true;
            }

            @Override
            public EnumScoreboardHealthDisplay c() {
                return EnumScoreboardHealthDisplay.INTEGER;
            }
        };
    }
}
