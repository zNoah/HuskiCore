package zkyubz.huskimc.core.huskicore.utils.nms.packets.player.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.player.WrappedNamedEntitySpawn;

import java.lang.reflect.Field;
import java.util.UUID;

public class WrappedNamedEntitySpawn_v1_8_R3 implements WrappedNamedEntitySpawn {
    private final Object packet;

    public WrappedNamedEntitySpawn_v1_8_R3(Object packet){
        this.packet = packet;
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.NAMED_ENTITY_SPAWN;
    }

    @Override
    public UUID playerUUID() {
        try {
            Field field = ((PacketPlayOutNamedEntitySpawn) packet).getClass().getDeclaredField("b");
            field.setAccessible(true);
            return (UUID) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }
}