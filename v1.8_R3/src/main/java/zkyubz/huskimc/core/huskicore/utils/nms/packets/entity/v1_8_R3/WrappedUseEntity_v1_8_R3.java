package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity;
import net.minecraft.server.v1_8_R3.World;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedUseEntity;

import java.lang.reflect.Field;

public class WrappedUseEntity_v1_8_R3 implements WrappedUseEntity {
    private final Object packet;

    public WrappedUseEntity_v1_8_R3(Object packet){
        this.packet = packet;
    }

    @Override
    public int getEntityId() {
        Integer entityId;
        try {
            Field field = packet.getClass().getDeclaredField("a");
            field.setAccessible(true);
            entityId = (Integer) field.get(packet);
        }
        catch (NoSuchFieldException | IllegalAccessException e){
            e.printStackTrace();
            return -1;
        }

        if (entityId != null){
            return entityId;
        }
        else {
            return -1;
        }
    }

    @Override
    public Object getNmsEntity(Object nmsWorld) {
        return ((PacketPlayInUseEntity) packet).a((World) nmsWorld);
    }

    @Override
    public int getAction() {
        return ((PacketPlayInUseEntity) packet).a().ordinal();
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.USE_ENTITY;
    }
}
