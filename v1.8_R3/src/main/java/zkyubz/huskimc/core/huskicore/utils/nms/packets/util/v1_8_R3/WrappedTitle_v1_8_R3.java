package zkyubz.huskimc.core.huskicore.utils.nms.packets.util.v1_8_R3;

import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.TitleAction;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.WrappedTitle;

public class WrappedTitle_v1_8_R3 implements WrappedTitle {
    private final Object packet;

    public WrappedTitle_v1_8_R3(String text, TitleAction titleAction){
        ChatComponentText textComponent = new ChatComponentText(text);
        PacketPlayOutTitle.EnumTitleAction enumTitleAction;

        switch (titleAction){
            case TITLE:
                enumTitleAction = PacketPlayOutTitle.EnumTitleAction.TITLE;
                break;
            case SUBTITLE:
                enumTitleAction = PacketPlayOutTitle.EnumTitleAction.SUBTITLE;
                break;
            case CLEAR:
                enumTitleAction = PacketPlayOutTitle.EnumTitleAction.CLEAR;
                break;
            case RESET:
                enumTitleAction = PacketPlayOutTitle.EnumTitleAction.RESET;
                break;
            default:
                enumTitleAction = null;
                break;
        }

        this.packet = new PacketPlayOutTitle(enumTitleAction, textComponent);
    }

    public WrappedTitle_v1_8_R3(int fadeIn, int stay, int fadeOut){
        this.packet = new PacketPlayOutTitle(fadeIn, stay, fadeOut);
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.TITLE;
    }
}
