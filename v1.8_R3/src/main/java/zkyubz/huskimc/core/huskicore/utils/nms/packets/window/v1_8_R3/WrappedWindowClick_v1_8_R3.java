package zkyubz.huskimc.core.huskicore.utils.nms.packets.window.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayInWindowClick;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.window.WrappedWindowClick;

public class WrappedWindowClick_v1_8_R3 implements WrappedWindowClick {
    private final Object packet;

    public WrappedWindowClick_v1_8_R3(Object packet){
        this.packet = packet;
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.WINDOW_CLICK;
    }

    @Override
    public int getWindowId() {
        return ((PacketPlayInWindowClick) packet).a();
    }

    @Override
    public int getSlot() {
        return ((PacketPlayInWindowClick) packet).b();
    }

    @Override
    public int getButtonPressed() {
        return ((PacketPlayInWindowClick) packet).c();
    }

    @Override
    public ItemStack getBukkitItemStack() {
        return CraftItemStack.asBukkitCopy(((PacketPlayInWindowClick) packet).e());
    }

    @Override
    public Object getNmsItemStack() {
        return ((PacketPlayInWindowClick) packet).e();
    }

    @Override
    public int getMode() {
        return ((PacketPlayInWindowClick) packet).f();
    }
}
