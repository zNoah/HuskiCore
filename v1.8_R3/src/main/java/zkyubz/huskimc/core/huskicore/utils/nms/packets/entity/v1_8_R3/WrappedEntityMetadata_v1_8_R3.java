package zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.v1_8_R3;

import net.minecraft.server.v1_8_R3.*;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.entity.WrappedEntityMetadata;

public class WrappedEntityMetadata_v1_8_R3 implements WrappedEntityMetadata {
    private final Object packet;
    private final Entity entityHolo;

    public WrappedEntityMetadata_v1_8_R3(String name, Object entity){
        ((Entity) entity).setCustomName(name);
        ((Entity) entity).setInvisible(true);
        ((Entity) entity).setCustomNameVisible(true);

        ((Entity) entity).getDataWatcher().watch(10, (byte) 16); //Is this Marker?

        DataWatcher data = ((Entity) entity).getDataWatcher();

        PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(((Entity) entity).getId(), data, true);
        this.entityHolo = (Entity) entity;
        this.packet = packet;
    }

    public WrappedEntityMetadata_v1_8_R3(String name, Object entity, boolean useMarker){
        ((Entity) entity).setCustomName(name);
        ((Entity) entity).setInvisible(true);
        ((Entity) entity).setCustomNameVisible(true);

        if (useMarker){
            ((Entity) entity).getDataWatcher().watch(10, (byte) 16); //Is this Marker?
        }

        DataWatcher data = ((Entity) entity).getDataWatcher();

        PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(((Entity) entity).getId(), data, true);
        this.entityHolo = (Entity) entity;
        this.packet = packet;
    }

    public WrappedEntityMetadata_v1_8_R3(Object entity){
        ((Entity) entity).setInvisible(true);
        ((Entity) entity).setCustomNameVisible(false);
        ((Entity) entity).getDataWatcher().watch(10, (byte) 16); //Is this Marker?

        DataWatcher data = ((Entity) entity).getDataWatcher();

        PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(((Entity) entity).getId(), data, true);
        this.entityHolo = (Entity) entity;
        this.packet = packet;
    }

    public WrappedEntityMetadata_v1_8_R3(Object entity, boolean useMarker){
        ((Entity) entity).setInvisible(true);
        ((Entity) entity).setCustomNameVisible(false);

        if (useMarker){
            ((Entity) entity).getDataWatcher().watch(10, (byte) 16); //Is this Marker?
        }

        DataWatcher data = ((Entity) entity).getDataWatcher();

        PacketPlayOutEntityMetadata packet = new PacketPlayOutEntityMetadata(((Entity) entity).getId(), data, true);
        this.entityHolo = (Entity) entity;
        this.packet = packet;
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.ENTITY_METADATA;
    }

    @Override
    public void update(String name) {
        //entityHolo.setCustomName(name);
        //DataWatcher data = entityHolo.getDataWatcher();

        //this.packet = new PacketPlayOutEntityMetadata(entityHolo.getId(), data, true);
    }
}
