package zkyubz.huskimc.core.huskicore.utils.nms.packets.util.v1_8_R3;

import net.minecraft.server.v1_8_R3.PacketPlayOutUpdateTime;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.PacketType;
import zkyubz.huskimc.core.huskicore.utils.nms.packets.util.WrappedUpdateTime;

import java.lang.reflect.Field;

public class WrappedUpdateTime_v1_8_R3 implements WrappedUpdateTime {
    private Object packet;

    //TODO: Check what is time and what is age! Might be the wrong way around
    public WrappedUpdateTime_v1_8_R3(long age, long time){
        this.packet = new PacketPlayOutUpdateTime(age, time, true); //What does true do?
    }

    public WrappedUpdateTime_v1_8_R3(long time){
        this.packet = new PacketPlayOutUpdateTime(time, time, true); //What does true do?
    }

    public WrappedUpdateTime_v1_8_R3(Object packet){
        this.packet = packet;
    }

    @Override
    public Object getPacket() {
        return packet;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.UPDATE_TIME;
    }

    @Override
    public long getTime() {
        try {
            Field field = ((PacketPlayOutUpdateTime) packet).getClass().getDeclaredField("b");
            field.setAccessible(true);
            return (long) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public long getAge() {
        try {
            Field field = ((PacketPlayOutUpdateTime) packet).getClass().getDeclaredField("a");
            field.setAccessible(true);
            return (long) field.get(packet);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
